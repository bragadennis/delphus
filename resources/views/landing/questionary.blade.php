@extends('layouts.site-header')

@section('content')
		<header>
			<div class="central">
				<div class="logo left">
					<!-- Olá! Bem-vindo(a). -->
					<img src="{!! url('/') !!}/front-jean/img/logo1.png" alt="iBYTE">
					<small class="block clear">Belém - PA</small>
				</div>

				<p class="right">Participe e concorra a uma Smart TV 4K.</p>
			</div>
		</header>

		<section>
			<div class="central">
				<div class="bloco br-10 overflow">
				<h3>Conectando com a ibyte_</h3>
				<p>A ibyte está chegando e já vai sortear uma <strong>Smart TV 4K</strong>.</p>        
				<p>Quer concorrer? Basta responder algumas perguntas.</p>

				<hr>

				<p><small><i class="fas fa-check"></i> Responda o questionário honestamente e sem pressa</small></p>
				<p><small><i class="fas fa-check"></i> Suas informações e opiniões são tratadas de forma confidencial</small></p>
				<p><small><i class="fas fa-check"></i> Responda o questionário até o fim</small></p>


				<div class="form">
					<!-- <form action=""> -->
					{!! Form::open(['url' => '/questionary', 'method' => 'post', 'class' => 'break-form']) !!}	
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Você, alguém de sua casa ou parente trabalha ou trabalhou em alguma dessas áreas?</p>
								<select name="question-1" id="question-1"  onchange="if(this.value=='encerrar'){encerraForm();}" required="required">
									<option value=""></option>
									<option value="encerrar">a - Agência de Propaganda ou Publicidade / Relações Públicas</option>
									<option value="encerrar">b - Instituto de Pesquisa de Mercado</option>
									<option value="encerrar">c – Loja de produtos de Informática</option>
									<option value="encerrar">d - Jornal / Revista / TV / Rádio</option>
									<option value="5">Nenhuma destas</option>
									<optgroup label=""></optgroup>
								</select>
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Onde você mora?</p>
								<select name="question-2" id="question-2"  onchange="if(this.value=='encerrar'){encerraForm();}" required="required">
									<option value=""></option>
									<option value="6">Belém</option>
									<option value="7">Ananindeua</option>
									<option value="8">Benevides</option>
									<option value="9">Marituba</option>
									<option value="10">Santa Izabel do Pará</option>
									<option value="11">Castanhal</option>
									<option value="encerrar">Outro</option>
									<optgroup label=""></optgroup>
								</select>
								<!-- <input type="text" placeholder="Caso 'outro', digite o nome da cidade."> -->
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<p>Qual é o seu gênero?</p>
							<label class="block padding-10">
								<input type="radio" name="question-3" value="13" class="left margin-hor-10" checked="checked">
								<span>Masculino</span>
							</label>
							<label class="block padding-10">
								<input type="radio" name="question-3" value="14" class="left margin-hor-10">
								<span>Feminino</span>
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Qual é a sua idade?</p>
								<input name='question-4' type="number" min="0" max="130" required="required">
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Qual é o seu grau de instrução?</p>
								<select name="question-5" id="question-5"  required="required">
									<option value=""></option>
									<option value="16">Ensino fundamental</option>
									<option value="17">Ensino médio</option>
									<option value="18">Superior incompleto</option>
									<option value="19">Superior completo/Pós-graduação</option>
									<optgroup label=""></optgroup>
								</select>
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Qual das opções abaixo retrata sua situação de trabalho atual?</p>
								<select name="question-6" id="question-6"  required="required">
									<option value=""></option>
									<option value="20">Emprego formal (com carteira assinada)</option>
									<option value="21">Emprego informal (sem carteira assinada)</option>
									<option value="22">Profissional autônomo</option>
									<option value="23">Emprego formal e emprego informal</option>
									<option value="24">Estou desempregado</option>
									<option value="25">Vivo de renda</option>
									<optgroup label=""></optgroup>
								</select>
							</label>
						</fieldset>		
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Em qual das opções abaixo retrata sua renda familiar mensal?</p>
								<select name="question-7" id="question-7"  required="required">
									<option value=""></option>
									<option value="26">Até 1 salário mínimo</option>
									<option value="27">Entre 1 e 2 salários</option>
									<option value="28">Entre 2 e 5 salários</option>
									<option value="29">Entre 5 e 10 salários</option>
									<option value="30">Acima de 10 salários mínimos</option>
									<option value="31">Não tenho renda</option>
									<optgroup label=""></optgroup>
								</select>
							</label>
						</fieldset>		
						<fieldset class="margin-ver-30">
							<p>Você utiliza ou compra produtos de tecnologia no seu dia a dia?</p>
							<label class="block padding-10">
								<input type="radio" name="question-8" value="32" class="left margin-hor-10">
								<span>Sim</span>
							</label>
							<label class="block padding-10">
								<input type="radio" name="question-8" value="33" class="left margin-hor-10" onchange="encerraForm()">
								<span>Não</span>
							</label>
						</fieldset>														
						<fieldset class="margin-ver-30">
							<p>Quais produtos de tecnologia MAIS UTILIZA no seu dia a dia?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="34">
								<small>Desktop/ Computador de mesa</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="35">
								<small>Notebook</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="36">
								<small>Televisão (sem internet)</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="37">
								<small>SmartTV</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="38">
								<small>Smartphone (celular com acesso à internet)</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="39">
								<small>Celular (sem acesso à internet)</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="40">
								<small>Bateria portátil</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="41">
								<small>Netbook</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="42">
								<small>Tablet</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="43">
								<small>GPS</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="44">
								<small>Impressoras</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="45">
								<small>Monitores</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="46">
								<small>Acessórios, periféricos e sortimentos de informática</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="47">
								<small>Mp3, Mp4, Mp5</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="48">
								<small>Câmeras Digitais</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="49">
								<small>Filmadoras</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="50">
								<small>Projetores Multimídia</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="51">
								<small>Home Theather/ Sound Bar</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="52">
								<small>Caixa de som portátil </small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="53">
								<small>Fones de ouvido, Headset</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="54">
								<small>Porta Retrato Digital</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="55">
								<small>Placas de Rede, Placa Mãe, Placa de Vídeo, Processador</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="56">
								<small>Estabilizadores</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="57">
								<small>Roteador, Switch, Hub, Modens</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="58">
								<small>Vídeo Games e Jogos</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-9[]" value="59">
								<small>Streaming de mídias (Chromecast, Apple TV)</small>
							</label>							
						</fieldset>		
						<fieldset class="margin-ver-30">
							<p>Quais destes produtos de tecnologia você COMPROU no último ano?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="60">
								<small>Desktop/ Computador de mesa</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="61">
								<small>Notebook</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="62">
								<small>Televisão (sem internet)</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="63">
								<small>SmartTV</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="64">
								<small>Smartphone (celular com acesso à internet)</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="65">
								<small>Celular (sem acesso à internet)</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="66">
								<small>Bateria portátil</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="67">
								<small>Netbook</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="68">
								<small>Tablet</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="69">
								<small>GPS</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="70">
								<small>Impressoras</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="71">
								<small>Monitores</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="72">
								<small>Acessórios, periféricos e sortimentos de informática</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="73">
								<small>Mp3, Mp4, Mp5</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="74">
								<small>Câmeras Digitais</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="75">
								<small>Filmadoras</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="76">
								<small>Projetores Multimídia</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="77">
								<small>Home Theather/ Sound Bar</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="78">
								<small>Caixa de som portátil </small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="79">
								<small>Fones de ouvido, Headset</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="80">
								<small>Porta Retrato Digital</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="81">
								<small>Placas de Rede, Placa Mãe, Placa de Vídeo, Processador</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="82">
								<small>Estabilizadores</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="83">
								<small>Roteador, Switch, Hub, Modens</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="84">
								<small>Vídeo Games e Jogos</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-10[]" value="85">
								<small>Streaming de mídias (Chromecast, Apple TV)</small>
							</label>		
						</fieldset>
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Quando falamos de local para comprar <strong>tecnologia</strong>, qual a primeira palavra que vem à mente?</p>
								<input name='question-11' type="text" placeholder="Digite aqui" required="required">
							</label>
						</fieldset>						
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Quais lojas de tecnologia você conhece mesmo que seja só de ouvir falar?</p>
								<input name='question-12' type="text" placeholder="Digite aqui" required="required">
								<label class="block padding-10 largura-100">
									<input type="checkbox" class="left margin-hor-10" name="quaislojasconhece" value="Nenhuma" onchange="if(this.value){encerraForm();}">
									<small>Nenhuma</small>
								</label>	
							</label>
						</fieldset>						
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Qual loja de tecnologia você <strong>mais compra</strong>?</p>
								<input name='question-13' type="text" placeholder="Digite aqui" required="required">
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<label class="block">
								<p>Em qual loja você comprou o seu <strong>último</strong> produto de tecnologia?</p>
								<input name='question-14' type="text" placeholder="Digite aqui" required="required">
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<p>Onde costuma realizar as compras de produto de tecnologia?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-15" value="90">
								<small>Lojas físicas</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-15" value="91">
								<small>Internet / Lojas online</small>
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<p>Costuma pesquisar sobre produtos de tecnologia antes da compra? Se sim, em quais meios?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="92">
								<small>Não pesquiso</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="93">
								<small>Amigos/Familiares</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="94">
								<small>Loja física</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="95">
								<small>Sites de lojas de tecnologia</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="96">
								<small>YouTube</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="97">
								<small>Facebook</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="98">
								<small>Instagram</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="99">
								<small>Blogs/ Sites especializados em tecnologia</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="100">
								<small>Panfletos</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-16" value="101">
								<small>Revista</small>
							</label>
							<!--<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="costumapesquisar" value="Outros">
								<small>Outros</small>
							</label> -->
							<input name='question-16-extra' type="text" placeholder="Outros">
						</fieldset>
						<fieldset class="margin-ver-30">
							<label class="block largura-100">
								<p>Em relação ao seu comportamento de compra de produtos de tecnologia, com qual das opções abaixo você mais se identifica?</p>
								<select name="question-17" required="required">
									<option value=""></option>
									<option value="102">Pesquiso na internet, mas prefiro comprar na loja física</option>
									<option value="103">Pesquiso e compro na loja física</option>
									<option value="104">Pesquisa e compro na loja virtual</option>
									<option value="105">Pesquiso na loja física e compro na loja virtual</option>
									<optgroup label=""></optgroup>
								</select>
							</label>
						</fieldset>
						<fieldset class="margin-ver-30">
							<p>Para uma loja de tecnologia, qual o grau de importância que você atribui para cada um dos fatores abaixo:</p>
							<hr>
							<q class="block align-c padding-10">Serviço de assistência técnica dentro da própria loja.</q>
							<div class="lista align-c block largura-100 padding-ver-10 overflow">
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-18" value="106">
										<strong>1</strong>
										<small class="block clear">não importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-18" value="107">
										<strong>2</strong>
										<small class="block clear">pouco importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-18" value="108">
										<strong>3</strong>
										<small class="block clear">importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-18" value="109">
										<strong>4</strong>
										<small class="block clear">muito importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-18" value="110">
										<strong>5</strong>
										<small class="block clear">extremamente importante</small>
									</label>
								</div>
							</div>
							<hr>
							<q class="block align-c padding-10">Oferta de serviços de manutenção, instalação e configuração de computadores, impressoras e redes sem fio dentro da própria loja.</q>
							<div class="lista align-c block largura-100 padding-ver-10 overflow">
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-19" value="111">
										<strong>1</strong>
										<small class="block clear">não importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-19" value="112">
										<strong>2</strong>
										<small class="block clear">pouco importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-19" value="113">
										<strong>3</strong>
										<small class="block clear">importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-19" value="114">
										<strong>4</strong>
										<small class="block clear">muito importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-19" value="115">
										<strong>5</strong>
										<small class="block clear">extremamente importante</small>
									</label>
								</div>
							</div>
							<hr>
							<q class="block align-c padding-10">Comprar produtos de tecnologia no site e retirar o produto na loja física.</q>
							<div class="lista align-c block largura-100 padding-ver-10 overflow">
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-20" value="116">
										<strong>1</strong>
										<small class="block clear">não importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-20" value="117">
										<strong>2</strong>
										<small class="block clear">pouco importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-20" value="118">
										<strong>3</strong>
										<small class="block clear">importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-20" value="119">
										<strong>4</strong>
										<small class="block clear">muito importante</small>
									</label>
								</div>
								<div class="left largura-20 mobile-div-2">
									<label class="block padding-10">
										<input type="radio" name="question-20" value="120">
										<strong>5</strong>
										<small class="block clear">extremamente importante</small>
									</label>
								</div>
							</div>
							<!-- <hr> -->
						</fieldset>
						<fieldset class="margin-ver-30">
							<p>Quais shoppings você mais frequenta?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-21[]" value="121">
								<small>Shopping a</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-21[]" value="122">
								<small>Shopping b</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-21[]" value="123">
								<small>Shopping c</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-21[]" value="124">
								<small>Shopping d</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-21[]" value="125">
								<small>Nenhum / Não frequento shoppings</small>
							</label>
							<input name='question-21-extra' type="text" placeholder="Outro">
						</fieldset>						
						<fieldset class="margin-ver-30">
							<p>Qual shopping é o seu preferido?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="radio" class="left margin-hor-10" name="question-22" value="126">
								<small>Shopping a</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="radio" class="left margin-hor-10" name="question-22" value="127">
								<small>Shopping b</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="radio" class="left margin-hor-10" name="question-22" value="128">
								<small>Shopping c</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="radio" class="left margin-hor-10" name="question-22" value="129">
								<small>Shopping d</small>
							</label>
							<input name='question-22-extra' type="text" placeholder="Outro">
						</fieldset>						
						<fieldset class="margin-ver-30">
							<p>Em que shopping você mais compra?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="radio" class="left margin-hor-10" name="question-23" value="130">
								<small>Shopping a</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="radio" class="left margin-hor-10" name="question-23" value="131">
								<small>Shopping b</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="radio" class="left margin-hor-10" name="question-23" value="132">
								<small>Shopping c</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="radio" class="left margin-hor-10" name="question-23" value="133">
								<small>Shopping d</small>
							</label>
							<input name='question-23-extra' type="text" placeholder="Outro">
						</fieldset>						
						<fieldset class="margin-ver-30">
							<p>Que atividades costuma realizar nos momentos de lazer?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="134">
								<small>Praia</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="135">
								<small>Futebol/Jogar bola</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="136">
								<small>Igreja</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="137">
								<small>Shopping</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="138">
								<small>Ver TV</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="139">
								<small>Ficar em casa</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="140">
								<small>Casa de amigos ou família</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="141">
								<small>Barzinhos</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" name="question-24[]" value="142">
								<small>Cinema</small>
							</label>		
							<input name='question-24-extra' type="text" placeholder="Outros">	
						</fieldset>
						<fieldset class="margin-ver-30">
							<p>Você lembra de ter visto, ouvido ou lido alguma propaganda de lojas de tecnologia no último mês?</p>
							<label class="block padding-10">
								<input type="radio" name="question-25" value="143" class="left margin-hor-10">
								<span>Sim</span>
							</label>
							<label class="block padding-10">
								<input type="radio" name="question-25" value="144" class="left margin-hor-10">
								<span>Não</span>
							</label>
						</fieldset>		
						<fieldset class="margin-ver-30">
							<p>Em quais meios de comunicação você viu, ouviu ou leu propagandas de lojas de tecnologia?</p>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-26[]" value="145">
								<small>TV aberta</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-26[]" value="146">
								<small>TV fechada</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-26[]" value="147">
								<small>Outdoor</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-26[]" value="148">
								<small>Busdoor (traseira de ônibus)</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-26[]" value="149">
								<small>Rádio</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-26[]" value="150">
								<small>Jornal</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-26[]" value="151">
								<small>Redes sociais</small>
							</label>
							<label class="block padding-10 largura-50 mobile-div-1 left">
								<input type="checkbox" class="left margin-hor-10" name="question-26[]" value="152">
								<small>Panfletos</small>
							</label>							
						</fieldset>
						{!! Form::hidden('lead_token', $lead->getToken(), ['id' => 'lead_token']) !!}
					{!! Form::close() !!}
					<!-- </form> -->
				</div>

<!-- 				<div class="botoes">
					<button class="largura-50 padding-15 botao left"><i class="fas fa-chevron-circle-left"></i> Voltar</button>
					<button class="largura-50 padding-15 botao right">Avançar <i class="fas fa-chevron-circle-right"></i></button>
				</div> -->

				</div>
			</div>
		</section>

		<div class="fone1 fixed"><img src="{!! url('/') !!}/front-jean/img/phone1.png" alt="Phone 1"></div>
		<div class="fone2 fixed"><img src="{!! url('/') !!}/front-jean/img/phone2.png" alt="Phone 2"></div>

		<div class="overlay fixed largura-100 altura-100">
			<div class="flex largura-100 altura-100">
				<div class="quadro relative">
					<div class="conteudo align-c">
						<h2>Obrigado.</h2>
						<p>Estamos encerrando o formulário de acordo com as suas respostas.</p>
						<p>Esta página será redirecionada em <span class="contador">7</span> segundos.</p>
					</div>
					<!-- <div class="fechar">x</div> -->
				</div>
			</div>
		</div>
@endsection