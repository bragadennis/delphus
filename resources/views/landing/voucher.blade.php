<!doctype html>
	<html class="no-js" lang="pt-br">

	<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Ibyte Promoção</title>
	<meta name="description" content="Concorra a uma Smart TV 4K">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="manifest" href="site.webmanifest">
	<link rel="apple-touch-icon" href="icon.png">
	<!-- Place favicon.ico in the root directory -->

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ url('/') }}/front-jean/css/reset.css">
	<link rel="stylesheet" href="{{ url('/') }}/front-jean/css/normalize.css">
	<link rel="stylesheet" href="{{ url('/') }}/front-jean/css/main.css">
	</head>

	<body>
		<header>
			<div class="central">
				<div class="logo left">
					<!-- Olá! Bem-vindo(a). -->
					<img src="{{ url('/') }}/front-jean/img/logo1.png" alt="IBYTE">
					<small class="block clear">Belém - PA</small>
				</div>

				<p class="right">Participe e concorra a uma Smart TV 4K.</p>
			</div>
		</header>

		<section>
			<div class="central">
				<div class="bloco br-10 overflow">
					<h3>Voucher</h3>
					<p>Este é o seu código para concorrer à Smart TV 4K.</p>	

					<div class="voucher relative">
						<div class="left largura-80 altura-100">
							<div class="helperLinha">
								<div class="auto">
									<span>Código:</span>
									<h4>{{ $lead->getVoucher() }}</h4>										
								</div>							
							</div>
						</div>
						<div class="right largura-20 altura-100">
							<div class="helperLinha">
								<div class="auto">
									<small class="block">Sorteio Smart TV 4K</small>
									<hr>
									<strong class="block">IBYTE Belém</strong>
								</div>
							</div>
						</div>
					</div>				
				</div>
			</div>
		</section>

		<div class="fone1 fixed"><img src="{{ url('/') }}/front-jean/img/phone1.png" alt="Phone 1"></div>
		<div class="fone2 fixed"><img src="{{ url('/') }}/front-jean/img/phone2.png" alt="Phone 2"></div>

		<script src="{{ url('/') }}/front-jean/js/vendor/modernizr-3.6.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
		<script src="{{ url('/') }}/front-jean/js/plugins.js"></script>
		<script src="{{ url('/') }}/front-jean/js/main.js"></script>

		<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
		<script>
		window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
		ga('create', 'UA-XXXXX-Y', 'auto'); ga('send', 'pageview')
		</script>
		<script src="https://www.google-analytics.com/analytics.js" async defer></script>
	</body>

</html>
