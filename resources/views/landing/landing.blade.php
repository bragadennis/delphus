@extends('layouts.site-header')

@section('content')
		<header>
			<div class="central">
				<div class="logo left">
					<!-- Olá! Bem-vindo(a). -->
					<img src="{!! url('/') !!}/front-jean/img/logo1.png" alt="iBYTE">
					<small class="block clear">Belém - PA</small>
				</div>

				<p class="right">Participe e concorra a uma Smart TV 4K.</p>
			</div>
		</header>

		<section>
			<div class="central">
				<div class="bloco br-10 overflow">
					@if( isset($status) AND  $status == 'invalid_token')
						<div class="alerta padding-15 margin-ver-10 br-10">
							<i class="far fa-bell"></i> <span class="margin-hor-5">Identificador de usuário inválido. Que tal <a href="{{ url('/')/lead }}">recomeçar o cadastro</a>?</span>
						</div>
					@endif

					<h3>Conectando com a ibyte_</h3>
					<p>A ibyte está chegando e já vai sortear uma <strong>Smart TV 4K</strong>.</p>        
					<p>Quer concorrer? Basta responder algumas perguntas.</p>

					<div class="form">
						{!! Form::open(['url' => '/lead', 'method' => 'post']) !!}
							<fieldset class="margin-ver-30">
								<label class="block">
									<p>
										<strong>Identifique-se.</strong> Preencha os campos abaixo com seus dados antes de responder o formulário.
									</p>

									{!! Form::text('fullname', null, ['placeholder' => 'Nome', 'required' => 'required']) !!}
									@if( $errors->has('fullname') )
										<span class='help-block'>
											<strong>{{ $errors->first('fullname') }}</strong>
										</span>
									@endif
									
									{!! Form::email('email', null, ['placeholder' => 'E-mail', 'required' => 'required']) !!}
									@if( $errors->has('email') )
										<span class='help-block'>
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
									
									{!! Form::text('phone', null, ['placeholder' => 'Número de celular com DDD', 'required' => 'required', 'class' => 'telefone']) !!}
									@if( $errors->has('phone') )
										<span class='help-block'>
											<strong>{{ $errors->first('phone') }}</strong>
										</span>
									@endif

									{!! Form::text('cpf', null, ['placeholder' => 'CPF', 'required' => 'required', 'class' => 'cpf']) !!}
									@if( $errors->has('cpf') )
										<span class='help-block'>
											<strong>{{ $errors->first('cpf') }}</strong>
										</span>
									@endif

									{!! Form::submit('Iniciar', ['class' => 'br-5 block largura-100 padding-15']) !!}
								</label>
							</fieldset>
						{!! Form::close() !!}
					</div>

				</div>
			</div>
		</section>

		<div class="fone1 fixed"><img src="{!! url('/') !!}/front-jean/img/phone1.png" alt="Phone 1"></div>
		<div class="fone2 fixed"><img src="{!! url('/') !!}/front-jean/img/phone2.png" alt="Phone 2"></div>

		<div class="overlay fixed largura-100 altura-100">
			<div class="flex largura-100 altura-100">
				<div class="quadro relative">
					<div class="conteudo align-c">
						<h2>Obrigado.</h2>
						<p>Estamos encerrando o formulário de acordo com as suas respostas.</p>
						<p>Esta página será redirecionada em <span class="contador">7</span> segundos.</p>
					</div>
					<!-- <div class="fechar">x</div> -->
				</div>
			</div>
		</div>
@endsection