@extends('layouts.app')

@section('content')
	<div>Lista Formulários</div>

	<div class='row'>
		<div class='col-md-4'>
			<h2>Lista Formulários</h2>
			<br>
		</div>
		<div class='col-md-6'></div>
		<div class='col-md-2' style='align:right;'>
			<a class='btn btn-success' href='{{ url("/form/create") }}'>
				<span class='glyphicon glyphicon-plus'></span> Formulário
			</a>
		</div>
	</div>

	<div id='form_list'>
		<table class='table table-condensed'>
			<thead>
				<th><center>ID</center></th>
				<th><center>Name</center></th>
				<th><center>Ordem</center></th>
				<th colspan='2'><center>Ações</center></th>
			</thead>
			<tbody>
				@foreach( $forms as $form )
					<tr id="form_{{ $form->id }}">
						<td><center>{{ $form->id }}</center></td>
						<td><center>{{ $form->getName() }}</center></td>
						<td><center>{{ $form->getNumber() }}</center></td>
						<td>
							<a href='{{ url("/form/$form->id/edit") }}'>
								<span class="glyphicon glyphicon-edit" aria-hidden='true'></span>
							</a>
						</td>
						<td>
							<a  href='#delete-register'
								class='deletable-register' 
								data-toggle='modal'
								data-id='{{ $form->id }}'
								data-target='#delete-register'>
							<span class="glyphicon glyphicon-trash" aria-hidden='true'></span>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection