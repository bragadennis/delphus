@extends('layouts.app')

@section('content')
	<div class='container-fluid'>
		<div class='row-fluid'>
			<div class='col-md-6 col-md-6-offset'>
				<h4>Cria/edita formulários</h4>
			</div>
		</div>
	</div>

	@if( isset( $form ) )
		{!! Form::model( $form, ['url' => "form/$form->id", 'method' => 'put'] ) !!}
	@else 
		{!! Form::open( ['url' => '/form', 'method' => 'post'] ) !!}
	@endif

	<br>
	<div class='container-fluid form-group'>
		<div class='container-fluid'>
			<div class='row-fluid'>
				{{-- Name --}}
					<div class='col-md-8'>
						<div class='col-md-2'>
							{!! Form::label('name', 'Nome') !!}
						</div>
						<div class='col-md-10'>
							{!! Form::text('name', null, ['class' => 'col-md-12 form-control']) !!}

							@if( $errors->has('name') )
								<span class='help-block'>
									<strong>{{ $errors->first('name') }}</strong>
								</span>
							@endif
						</div>
					</div>

				{{-- Number --}}
					<div class='col-md-4'>
						<div class='col-md-3'>
							{!! Form::label('number', 'Número') !!}
						</div>
						<div class='col-md-9'>
							{!! Form::number('number', null, ['class' => 'col-md-12 form-control']) !!}

							@if( $errors->has('number') )
								<span class='help-block'>
									<strong>{{ $errors->first('number') }}</strong>
								</span>
							@endif
						</div>
					</div>
			</div>
		</div>
		<br>

		<div class='row-fluid'>
			<div class='col-md-3 col-md-offset-9'>
				<a href='{{ url("form") }}' class='btn btn-warning'>
					<span class='glyphicon glyphicon-chevron-left'></span>
					Voltar
				</a>
				{!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
			</div>
		</div>			
	</div>
@endsection