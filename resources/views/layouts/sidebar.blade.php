<div class='row'>
	<div class='sidebar-nav'>
		<ul class='nav nav-list'>
			{{-- Adminstração --}}
				<li class='nav-header'>Administrar</li>

				@php
					$side_bar_list = array(
						'dashboard' => 'Dashboard',
						'form' => 'Formulários',
						'question' => 'Perguntas',
					);
				@endphp

				@foreach($side_bar_list as $url => $name )
					@if( isset($active) AND $active == $url )
						<li class="active"><a href='{!! url("$url") !!}'>{{ $name }}</a></li>
					@else
						<li><a href='{!! url("$url") !!}'>{{ $name }}</a></li>
					@endif
				@endforeach
    	</ul>
    </div>
</div>