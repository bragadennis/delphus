
{{-- Show this when there's a indistinguable error in need of attention --}}
{{-- show-to-the-it-guy --}}
	<div class="modal fade" id="show-to-the-it-guy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog modal-lg" role="document">
	  		<div class="modal-content">
	  		  	<div class="modal-header">
	  		  	  	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  		  	  	<h4 class="modal-title" id="myModalLabel">Houve um erro na requisição</h4>
	  		  	</div>
	  		  	<div class="modal-body">
	  		  	  	Provavelmente uma ação gerou uma requisição mal formulada. Por consequência disso, o pedido atual não pode ser processado.<br><br>

	  		  	  	Mostre isso ao rapaz da TI, ele saberá o que fazer:<br>
	  		  	  	<code id='show-to-the-it-guy-text-message'></code>
	  		  	</div>
	  		  	<div class="modal-footer">
	  		  	  	<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
	  		  	</div>
	  		</div>
	  	</div>
	</div>

{{-- Show this when the user is about the delete or alter an register (level: warning) --}}
	<div class="modal fade" id="show-alter-and-delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  	<div class="modal-dialog" role="document">
	  		<div class="modal-content">
	  		  	<div class="modal-header">
	  		  	  	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  		  	  	<h4 class="modal-title" id="myModalLabel">Essa operação não pode ser desfeita!!!</h4>
	  		  	</div>
	  		  	<div class="modal-body">
					{{ Form::hidden('resource', null, ['id' => 'modal-operation-resource-path']) }}
					{{ Form::hidden('verb',     null, ['id' => 'modal-operation-resource-verb']) }}
					{{ Form::hidden('payload',  null, ['id' => 'modal-operation-resource-payload']) }}

	  		  	  	Você está prestes a realização uma operação que <strong>não pode ser revertida!</strong><br>
	  		  	  	Operação: <span id='modal-operation-span-note'></span> <br> <br>

	  		  	  	<strong>Deseja continuar com a ação assim mesmo?</strong>
	  		  	</div>
	  		  	<div class="modal-footer">
	  		  	  	<button type="button" class="btn btn-danger  btn-xs" id='confirm-danger-modal-operation'>Continuar</button>
	  		  	  	<button type="button" class="btn btn-success btn-lg" data-dismiss="modal">Cancelar</button>
	  		  	</div>
	  		</div>
	  	</div>
	</div>