@extends('layouts.app')

@section('content')
	<h4>Ver pergunta</h4>

	<div class='container-fluid'>
		<br>
		<div class='row-fluid'>
			<div class='col-md-12'>
				<strong>Formulário: </strong> {{ $question->getForm()->id . " - " . $question->getForm()->getName() }}
			</div>
		</div>
		<br><br>
		<div class='row-fluid'>
			<div class='col-md-6'>
				<strong>Pergunta: </strong> {{ $question->getQuestion() }}
			</div>
			<div class='col-md-3'>
				<strong>Ordem: </strong> {{ $question->getOrder() }}º
			</div>
			<div class='col-md-3'>
				<strong>Tipo do Campo: </strong> {{ $question->getComprehensiveType() }}
			</div>
		</div>

		<br><br>
		<div class='row-fluid'>
			<div class='col-md-3 col-md-offset-9'>
				<a href='{{ url("question") }}' class='btn btn-warning'>
					<span class='glyphicon glyphicon-chevron-left'></span>
					Voltar
				</a>
				<a href='{{ url("question/$question->id/edit") }}' class='btn btn-success'>
					<span class='glyphicon glyphicon-pencil'></span>
					Editar
				</a>
			</div>
		</div>
	</div>

	<h4>Manter opções da pergunta</h4>
	<div class='container-fluid quadro'>
		<table class='table table-striped table-condensed'>
			<thead>
				<th>ID</th>
				<th>Resposta</th>
				<th>Ordem</th>
				<th>É bloqueante?</th>
				<th colspan=2><center>Ações</center></th>
			</thead>
			<tbody>	
				@foreach( $question->getOptionsOnOrder() as $option )
					<tr id='option_for_question_{{ $question->id }}'>
						<td><center>{{ $option->id }}</center></td>
						<td><center>{{ $option->getAnswer() }}</center></td>
						<td><center>{{ $option->getOrder() }}º</center></td>
						<td><center>{{ ( $option->isBlocker() ? 'Sim' : 'Não' ) }}</center></td>
						<td>
							<center>
								<a  href='#edit-option'
									data-toggle='modal'
									data-target='#edit-option-modal'
									data-option-id='{{ $option->id }}'
									data-question-id='{{ $question->id }}'
									data-option-answer="{{ $option->getAnswer() }}"
									data-option-order="{{ $option->getOrder() }}"
									data-option-blocker="{{ ( $option->isBlocker() ) ? 'true' : 'false' }}"
									class='edit-question-option'>
									<span class="glyphicon glyphicon-pencil" aria-hidden='true'></span>
								</a>
							</center>
						</td>
						<td>
							<center>
								<a  href='#remove-option'
									data-toggle='modal'
									data-question-id='{{ $question->id }}'
									data-option-id='{{ $option->id }}'
									data-target='#delete-option-modal'
									class='deletable-option-register'>
									<span class="glyphicon glyphicon-trash" aria-hidden='true'></span>
								</a>
							</center>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<div class='row-fluid'>
			<div class='col-md-3 col-md-offset-9'>
				<a  href='#add-option'
					data-toggle='modal'
					data-target='#add-option-modal'
					class='btn btn-warning'>
					<span class='glyphicon glyphicon-plus'></span>Adicionar Opção
				</a>
			</div>
		</div>
	</div>

	{{-- Modals --}}
		{{-- Include option modal --}}
			<div id="add-option-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class='modal-dialog' role='document' style='width: 80%;'>
					<div class='modal-content'>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h3 id="myModalLabel">Adicionar Opção</h3>
						</div>
						<div class="modal-body">
							{{-- Fields to display --}}
								<div class='container-fluid'>
									<div class='row-fluid'>
										{{-- Question --}}
											<div class='col-md-1'>
												{!! Form::label(null, 'Pergunta') !!}
											</div>
											<div class='col-md-3 display-question-name'>
												@if( isset($question) )
													{{ $question->getQuestion() }}
												@endif
											</div>
										{{-- Order --}}
											<div class='col-md-2'>
												{!! Form::label(null, 'Ordem da Pergunta') !!}
											</div>
											<div class='col-md-2 display-question-order'>
												@if( isset($question) )
													{{ $question->getOrder() }}
												@endif
											</div>
										{{-- Type --}}
											<div class='col-md-1'>
												{!! Form::label(null, 'Tipo') !!}
											</div>
											<div class='col-md-3 display-question-type'>
												@if( isset($question) )
													{{ $question->getComprehensiveType() }}
												@endif
											</div>

											<!-- Question's ID -->
											{!! Form::hidden('question_id', $question->id) !!}
									</div>
								</div>
								<br>

							{{-- Fields to fullfil --}}
								<div class='container-fluid'>
									<div class='row-fluid'>
										{{-- Answer --}}
											<div class='col-md-2'>
												{!! Form::label('option', "Resposta/Opção") !!}
											</div>
											<div class='col-md-3'>
												{!! Form::text('option', null, ['id' => 'option-answer',
																				'class' => 'col-md-12 form-control']) !!}
											</div>

										{{-- Order --}}
											<div class='col-md-1'>
												{!! Form::label('order', "Ordem") !!}
											</div>
											<div class='col-md-2'>
												{!! Form::number('order', null, ['id' => 'option-order',
																  				 'class' => 'col-md-12 form-control']) !!}
											</div>

										{{-- Blocker --}}
											<div class='col-md-1'>
												{!! Form::label('blocker', "Bloqueante") !!}
											</div>
											<div class='col-md-2'>
												{!! Form::select('blocker', 
																 ['0' => 'Não', '1' => 'Sim'],
																 null, 
																 ['id' => 'option-blocker',
																 'class' => 'col-md-12 form-control']) !!}
											</div>
									</div>
								</div>
								<br>

							{!! Form::hidden('question_id',   null, ['id' => 'modal_question_id']) !!}
						</div>
						<div class="modal-footer">
							<button id="add-option-confirm" class="btn btn-small btn-success" data-dismiss="modal" aria-hidden="true">Adicionar</button>
							<button class="btn btn-small btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</button>
						</div>
					</div>
				</div>
			</div>
		
		{{-- Edit question's option modal --}}
			<div id="edit-option-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class='modal-dialog' role='document' style='width: 80%;'>
					<div class='modal-content'>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h3 id="myModalLabel">Adicionar Opção</h3>
						</div>
						<div class="modal-body">
							{{-- Fields to display --}}
								<div class='container-fluid'>
									<div class='row-fluid'>
										{{-- Question --}}
											<div class='col-md-1'>
												{!! Form::label(null, 'Pergunta') !!}
											</div>
											<div class='col-md-3 display-question-name'>
												@if( isset($question) )
													{{ $question->getQuestion() }}
												@endif
											</div>
										{{-- Order --}}
											<div class='col-md-2'>
												{!! Form::label(null, 'Ordem da Pergunta') !!}
											</div>
											<div class='col-md-2 display-question-order'>
												@if( isset($question) )
													{{ $question->getOrder() }}
												@endif
											</div>
										{{-- Type --}}
											<div class='col-md-1'>
												{!! Form::label(null, 'Tipo') !!}
											</div>
											<div class='col-md-3 display-question-type'>
												@if( isset($question) )
													{{ $question->getComprehensiveType() }}
												@endif
											</div>

											<!-- Question's ID -->
											{!! Form::hidden('question_id', $question->id) !!}
									</div>
								</div>
								<br>

							{{-- Fields to fullfil --}}
								<div class='container-fluid'>
									<div class='row-fluid'>
										{{-- Answer --}}
											<div class='col-md-2'>
												{!! Form::label('option', "Resposta/Opção") !!}
											</div>
											<div class='col-md-3'>
												{!! Form::text('option', null, ['id' => 'edit-option-answer',
																				'class' => 'col-md-12 form-control']) !!}
											</div>

										{{-- Order --}}
											<div class='col-md-1'>
												{!! Form::label('order', "Ordem") !!}
											</div>
											<div class='col-md-2'>
												{!! Form::number('order', null, ['id' => 'edit-option-order',
																  				 'class' => 'col-md-12 form-control']) !!}
											</div>

										{{-- Blocker --}}
											<div class='col-md-1'>
												{!! Form::label('blocker', "Bloqueante") !!}
											</div>
											<div class='col-md-2'>
												{!! Form::select('blocker', 
																 ['0' => 'Não', '1' => 'Sim'],
																 null, 
																 ['id' => 'edit-option-blocker',
																 'class' => 'col-md-12 form-control']) !!}
											</div>
									</div>
								</div>
								<br>

							{!! Form::hidden('question_id', null, ['id' => 'modal_question_id']) !!}
							{!! Form::hidden('option_id',   null, ['id' => 'modal_option_id']) !!}
						</div>
						<div class="modal-footer">
							<button id="edit-option-confirm" class="btn btn-small btn-success" data-dismiss="modal" aria-hidden="true">Adicionar</button>
							<button class="btn btn-small btn-danger" data-dismiss="modal" aria-hidden="true">Cancelar</button>
						</div>
					</div>
				</div>
			</div>

		{{-- Remove question's option modal --}}
			<div id="delete-option-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class='modal-dialog' role='document' style='width: 80%;'>
					<div class='modal-content'>
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h3 id="myModalLabel">Remover Opção</h3>
						</div>
						<div class="modal-body">
							<p>Deseja realmente deletar o opção? Ela <strong>não</strong> poderá ser recuperada e <strong>todas</strong> as informações adicionais serão perdidas <strong>permanentemente!</strong></p>

							{!! Form::hidden('delete_question_id', null, ['id' => 'modal_question_id']) !!}
							{!! Form::hidden('delete_option_id',   null, ['id' => 'modal_option_id']) !!}
						</div>	
						<div class="modal-footer">
							<button id="delete-option-confirm" class="btn btn-small" data-dismiss="modal" aria-hidden="true">Deletar</button>
							<button class="btn btn-md btn-success" data-dismiss="modal" aria-hidden="true">Cancelar</button>
						</div>
					</div>
				</div>
			</div>
@endsection