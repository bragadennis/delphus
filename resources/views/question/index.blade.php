@extends('layouts.app')

@section('content')
	<div>Lista Perguntas</div>

	<div class='row'>
		<div class='col-md-4'>
			<h2>Lista Perguntas</h2>
			<br>
		</div>
		<div class='col-md-6'></div>
		<div class='col-md-2' style='align:right;'>
			<a class='btn btn-success' href='{{ url("/question/create") }}'>
				<span class='glyphicon glyphicon-plus'></span> Pergunta
			</a>
		</div>
	</div>

	<div id='question_list'>
		<table class='table table-condensed'>
			<thead>
				<th><center>ID</center></th>
				<th><center>Pergunta</center></th>
				<th><center>Ordem</center></th>
				<th><center>Tipo</center></th>
				<th><center>Formulário</center></th>
				<th colspan='3'><center>Ações</center></th>
			</thead>
			<tbody>
				@foreach( $questions as $question )
					<tr id="question_{{ $question->id }}">
						<td><center>{{ $question->id }}</center></td>
						<td><center>{{ $question->getQuestion() }}</center></td>
						<td><center>{{ $question->getOrder() }}</center></td>
						<td><center>{{ $question->getComprehensiveType() }}</center></td>
						<td><center>{{ $question->getForm()->getName() }}</center></td>
						<td>
							<a href='{{ url("/question/$question->id") }}'>
								<span class="glyphicon glyphicon-eye-open" aria-hidden='true'></span>
							</a>
						</td>
						<td>
							<a href='{{ url("/question/$question->id/edit") }}'>
								<span class="glyphicon glyphicon-edit" aria-hidden='true'></span>
							</a>
						</td>
						<td>
							<a  href='#delete-register'
								class='deletable-register' 
								data-toggle='modal'
								data-id='{{ $question->id }}'
								data-target='#delete-register'>
							<span class="glyphicon glyphicon-trash" aria-hidden='true'></span>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	{{-- Remove question's option modal --}}
		<div id="delete-register" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class='modal-dialog' role='document' style='width: 80%;'>
				<div class='modal-content'>
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h3 id="myModalLabel">Remover Opção</h3>
					</div>
					<div class="modal-body">
						<p>Deseja realmente deletar o opção? Ela <strong>não</strong> poderá ser recuperada e <strong>todas</strong> as informações adicionais serão perdidas <strong>permanentemente!</strong></p>

						{!! Form::hidden('delete_question_id', null, ['id' => 'modal_question_id']) !!}
					</div>	
					<div class="modal-footer">
						<button id="delete-question-confirm" class="btn btn-small" data-dismiss="modal" aria-hidden="true">Deletar</button>
						<button class="btn btn-md btn-success" data-dismiss="modal" aria-hidden="true">Cancelar</button>
					</div>
				</div>
			</div>
		</div>
@endsection