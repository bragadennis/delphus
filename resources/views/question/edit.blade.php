@extends('layouts.app')

@section('content')
	<div class='container-fluid'>
		<div class='row-fluid'>
			<div class='col-md-6 col-md-6-offset'>
				<h4>Cria/edita perguntas</h4>
			</div>
		</div>
	</div>

	@if( isset( $question ) )
		{!! Form::model( $question, ['url' => "question/$question->id", 'method' => 'put'] ) !!}
	@else 
		{!! Form::open( ['url' => '/question', 'method' => 'post'] ) !!}
	@endif

	<br>
	<div class='container-fluid form-group'>
		<div class='container-fluid'>
			<div class='row-fluid'>
				{{-- Select form --}}
					<div class='col-md-2'>
						{!! Form::label('form_id', 'Formulário') !!}
					</div>
					<div class='col-md-10'>
						{!! Form::select('form_id', $forms, null, ['class' => 'col-md-5 form-control']) !!}
					</div>
			</div>
			<br><br><br>

			<div class='row-fluid'>
				{{-- Question --}}
					<div class='col-md-8'>
						<div class='col-md-2'>
							{!! Form::label('question', 'Pergunta') !!}
						</div>
						<div class='col-md-10'>
							{!! Form::text('question', null, ['class' => 'col-md-12 form-control']) !!}

							@if( $errors->has('question') )
								<span class='help-block'>
									<strong>{{ $errors->first('question') }}</strong>
								</span>
							@endif
						</div>
					</div>

				{{-- Order --}}
					<div class='col-md-4'>
						<div class='col-md-3'>
							{!! Form::label('order', 'Ordem') !!}
						</div>
						<div class='col-md-9'>
							{!! Form::number('order', null, ['class' => 'col-md-12 form-control']) !!}

							@if( $errors->has('order') )
								<span class='help-block'>
									<strong>{{ $errors->first('order') }}</strong>
								</span>
							@endif
						</div>
					</div>
			</div>
			<br><br><br>

			<div class='row-fluid'>
				{{-- Type --}}
					<div class='col-md-2'>
						{!! Form::label('type', 'Tipo do Campo') !!}
					</div>
					<div class='col-md-6'>
						{!! Form::select('type', $types, null, ['class' => 'col-md-12 form-control']) !!}

						@if( $errors->has('type') )
							<span class='help-block'>
								<strong>{{ $errors->first('type') }}</strong>
							</span>
						@endif
					</div>
			</div>
		</div>
	</div>

	<div class='row-fluid'>
		<div class='col-md-3 col-md-offset-9'>
			<a href='{{ url("question") }}' class='btn btn-warning'>
				<span class='glyphicon glyphicon-chevron-left'></span>
				Voltar
			</a>
			{!! Form::submit('Salvar', ['class' => 'btn btn-success']) !!}
		</div>
	</div>			
@endsection