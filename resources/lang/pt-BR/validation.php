<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Precisa ser aceito.',
    'active_url'           => 'URL não é valida.',
    'after'                => 'O campo precisa ser uma data maior que :date.',
    'after_or_equal'       => 'O campo precisa ser maior ou igual a :date.',
    'alpha'                => 'O campo deve conter somente letras.',
    'alpha_dash'           => 'O campo deve conter somente letras, números e barras.',
    'alpha_num'            => 'O campo deve conter somente letras e números.',
    'array'                => 'O campo precisa ser um array.',
    'before'               => 'O campo precisa ser uma data menor que :date.',
    'before_or_equal'      => 'O campo precisa ser uma data menor ou igual a :date.',
    'between'              => [
        'numeric' => 'O campo precisa ser entre :min e :max.',
        'file'    => 'O campo precisa ser entre :min e :max kb.',
        'string'  => 'O campo precisa ser :min e :max caracteres.',
        'array'   => 'O campo precisa ter :min e :max itens.',
    ],
    'boolean'              => 'O campo precisa ser verdadeiro ou falso.',
    'confirmed'            => 'Os campos não são iguais.',
    'date'                 => 'O campo não é uma data válida.',
    'date_format'          => 'O campo não corresponde o formato :format.',
    'different'            => 'O campo e :other precisa ser diferente.',
    'digits'               => 'O campo precisa ser :digits digitos.',
    'digits_between'       => 'O campo precisa ser ntre :min e :max digitos.',
    'dimensions'           => 'O campo tem dimensões inválidas.',
    'distinct'             => 'O campo campo tem um valor duplicado.',
    'email'                => 'O campo precisa ser um e-mail válido.',
    'exists'               => 'O campo selecionado é inválido.',
    'file'                 => 'O campo precisa ser um arquivo.',
    'filled'               => 'O campo precisa ser preenchido.',
    'image'                => 'O campo precisa ser uma imagem.',
    'in'                   => 'O campo selecionado é inválido.',
    'in_array'             => 'O campo não exite em :other.',
    'integer'              => 'O campo precisa ser inteiro.',
    'ip'                   => 'O campo precisa ser um endereço de IP válido.',
    'ipv4'                 => 'O campo precisa ser um endereço IPv4 válido.',
    'ipv6'                 => 'O campo precisa ser um endereço IPv6 válido.',
    'json'                 => 'O campo precisa ser um JSON válido.',
    'max'                  => [
        'numeric' => 'O campo não deve ser maior que :max.',
        'file'    => 'O campo não deve ser maior que :max kb.',
        'string'  => 'O campo não deve ser maior que :max caracteres.',
        'array'   => 'O campo não pode ter mais que :max itens.',
    ],
    'mimes'                => 'O campo precisa ser um arquivo do tipo: :values.',
    'mimetypes'            => 'O campo precisa ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O campo precisa ter pelo menos :min.',
        'file'    => 'O campo precisa ter pelo menos :min kb.',
        'string'  => 'O campo precisa ter pelo menos :min caracteres.',
        'array'   => 'O campo precisa ter pelo menos :min itens.',
    ],
    'not_in'               => 'O campo é inválido.',
    'numeric'              => 'O campo precisa ser um número.',
    'present'              => 'O campo precisa ser presente.',
    'regex'                => 'O campo formato é inválido.',
    'required'             => 'O campo é obrigatório.',
    'required_if'          => 'O campo é obrigatório quando :other é :value.',
    'required_unless'      => 'O campo é obrigatório ao menos que :other esta dentro :values.',
    'required_with'        => 'O campo é obrigatório quando :values é presente.',
    'required_with_all'    => 'O campo é obrigatório quando :values é presente.',
    'required_without'     => 'O campo é obrigatório quando :values não ta presente.',
    'required_without_all' => 'O campo é obrigatório quando nenhum dos :values estão presentes.',
    'same'                 => 'Os campos precisam ser iguais.',
    'size'                 => [
        'numeric' => 'O campo precisa ser :size.',
        'file'    => 'O campo precisa ser :size kb.',
        'string'  => 'O campo precisa ser :size caracteres.',
        'array'   => 'O campo precisa conter :size itens.',
    ],
    'string'               => 'O campo precisa ser uma string.',
    'timezone'             => 'O campo precisa ser uma zona válida.',
    'unique'               => 'O campo já existente..',
    'uploaded'             => 'O campo falhou o envio.',
    'url'                  => 'O campo formato é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
