<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Senhas precisam ter pelo menos 6 caracteres e serem compatíveis',
    'reset' => 'Seu password foi resetado!',
    'sent' => 'Nós lhe encaminhamos um link para redefinição de senha!',
    'token' => 'Esse link de redefinição de senha é inválido.',
    'user' => "Nós não conseguimos encontrar usuário associado com esse e-mail.",
];
