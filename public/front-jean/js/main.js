$(document).ready(function(){

	var fdst = $(".break-form fieldset");
	for(var i = 0; i < fdst.length; i+=8) {
		fdst.slice(i, i+8).wrapAll("<div class='slide'></div>");
	};
	setTimeout(function(){
		$('.slide:eq(0)').fadeIn();
	},500);

	var qntSlides = $('.slide').length;
	$('.slide').each(function(index){
		// console.log($(this).index())
		if($(this).index() == 1){
			$(this).append('<div class="btn largura-50 padding-15 align-c right">Avançar ></div>')
		} else if($(this).index() == qntSlides) {
			$(this).append('<div class="btn largura-50 padding-15 align-c left">< Voltar</div><input class="block padding-15 right largura-50" type="submit" value="Enviar">')
		} else {
			$(this).append('<div class="btn largura-50 padding-15 align-c left">< Voltar</div><div class="btn largura-50 padding-15 align-c right">Avançar ></a>')
		}
	});
	// console.log(qntSlides);

	// MASCARAS
	$('.telefone').mask('(00)00000-0000');
	$('.cpf').mask('000.000.000-00', {reverse: true});

	var slideIndex = 0;
	$('.btn, form input[type="submit"]').on('click',function(){
		// botao avançar
		if($(this).hasClass('right')){
			var validado = true;
			$('section .bloco .slide:visible *[required="required"]').each(function(){
				var valorDeCada = $(this)[0].value;
				// console.log(valorDeCada);
				if(!valorDeCada || valorDeCada==0 || valorDeCada=="" || valorDeCada==false){
					validado = false;
					$(this).parents('fieldset').addClass('erro');
				} else {
					$(this).parents('fieldset').removeClass('erro');
				}
			});
			if(validado == false){
				alert('Um ou mais campos obrigatórios não foram preenchidos. Volte ao formulário e revise-o.');
			} else {
				
				if(slideIndex < ($('section .bloco .slide').length - 1)){
					slideIndex++;
				}
				$('section .bloco .slide:visible').fadeOut(0);
				$('section .bloco .slide:eq('+slideIndex+')').fadeIn(0);
				
				
				if((slideIndex+1) == $('section .bloco .slide').length){
					
				}				
			}
		
		} else {
			if(slideIndex > 0){
				slideIndex--;
			}
			$('section .bloco .slide:visible').fadeOut(0);
			$('section .bloco .slide:eq('+slideIndex+')').fadeIn(0);			
		}
		$("html, body").stop().animate({scrollTop:0}, 500);
	});

});


function encerraForm(){
	$('.overlay').fadeIn();
	var lead_token = $('#lead_token').val();

	// request to bock user.
	$.ajax({
		url: BASE_URL + '/lead/block/' + lead_token, 
		type: 'GET'
	}).done(function(result) {
		if( result != 200 )
			alert('Alerta ao atualizar informações do usuário. Repetir a tentativa, por favor!');
	});

	var cont = 0;
	var contagem = setInterval(function(){
		cont++;
		if(cont > 7){
			// desabilita o form
			$('body form').find('input,select').attr("disabled","disabled");
			// limpa o contador
			clearInterval(contagem);
			// redireciona para uma page qualquer
			window.location = "http://www.google.com";
		}
		$('.contador').html(8-cont);
	},1000);
	contagem();
}