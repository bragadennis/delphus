@extends('layouts.site-header')

@section('content')

		<!--
 		<section>
			<div>
				<h3>Conectando com a ibyte_</h3>
				<p>A ibyte está chegando e já vai soretar uma Smart TV 4K.</p>        
				<p>Quer concorrer? Basta responder algumas perguntas.</p>        
			</div>
		</section> 
		-->

		<header>
			<div class="central">
				<div class="logo left">
					<!-- Olá! Bem-vindo(a). -->
					<img src="img/logo1.png" alt="iBYTE">
					<small class="block clear">Belém - PA</small>
				</div>

				<p class="right">Participe e concorra a uma Smart TV 4K.</p>

				<!--
				<nav class="right">
					<ul>
						<li><a href="#">Questionário</a></li>
						<li><a href="#">Sorteio</a></li>
						<li><a href="#">Voucher</a></li>
						<li><a href="#">Confirmação</a></li>
					</ul>
				</nav>	 
				-->
			</div>
		</header>

		<section>
			<div class="central">
				<div class="bloco br-10 overflow">
				<h3>Conectando com a ibyte_</h3>
				<p>A ibyte está chegando e já vai sortear uma <strong>Smart TV 4K</strong>.</p>        
				<p>Quer concorrer? Basta responder algumas perguntas.</p>

				<div class="form">
					<form action="">
						<fieldset class="margin-ver-30">
							<label class="block">
								<p><strong>Identifique-se.</strong> Preencha os campos abaixo com seus dados antes de responder o formulário.</p>
								<input type="text" placeholder="Digite seu nome">
								<input type="email" placeholder="Digite seu e-mail">
								<input type="submit" value="Iniciar" class="br-5 block largura-100 padding-15">
							</label>
						</fieldset>
					</form>
				</div>

				</div>
			</div>
		</section>

		<div class="fone1 fixed"><img src="img/phone1.png" alt="Phone 1"></div>
		<div class="fone2 fixed"><img src="img/phone2.png" alt="Phone 2"></div>

		<div class="overlay fixed largura-100 altura-100">
			<div class="flex largura-100 altura-100">
				<div class="quadro relative">
					<div class="conteudo align-c">
						<h2>Obrigado.</h2>
						<p>Estamos encerrando o formulário de acordo com as suas respostas.</p>
						<p>Esta página será redirecionada em <span class="contador">7</span> segundos.</p>
					</div>
					<!-- <div class="fechar">x</div> -->
				</div>
			</div>
		</div>
@endsection