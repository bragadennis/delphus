$(document).ready(function(){

	var fdst = $("fieldset");
	for(var i = 0; i < fdst.length; i+=8) {
		fdst.slice(i, i+8).wrapAll("<div class='slide'></div>");
	}

	var slideIndex = 0;
	$('.botoes .botao').on('click',function(){
		// botao avançar
		if($(this).hasClass('right')){
			var validado = true;
			$('section .bloco .slide:visible *[required="required"]').each(function(){
				var valorDeCada = $(this)[0].value;
				if(!valorDeCada){
					validado = false;
					$(this).parents('fieldset').addClass('erro');
				} else {
					$(this).parents('fieldset').removeClass('erro');
				}
			});
			if(validado == false){
				alert('Um ou mais campos obrigatórios não foram preenchidos. Volte ao formulário e revise-o.');
			} else {
				// if(slideIndex < $('section .bloco .slide').length){
				if(slideIndex < ($('section .bloco .slide').length - 1)){
					slideIndex++;
				}
				$('section .bloco .slide:visible').fadeOut(0);
				$('section .bloco .slide:eq('+slideIndex+')').fadeIn(0);
				
				// momento de envio do form
				if((slideIndex+1) == $('section .bloco .slide').length){
					// alert('fim do slideshow');
				}				
			}
		// botao recuar
		} else {
			if(slideIndex > 0){
				slideIndex--;
			}
			$('section .bloco .slide:visible').fadeOut(0);
			$('section .bloco .slide:eq('+slideIndex+')').fadeIn(0);			
		}
		$("html, body").stop().animate({scrollTop:0}, 500);
	});

});


function encerraForm(){
	$('.overlay').fadeIn();
	var cont = 0;
	var contagem = setInterval(function(){
		cont++;
		if(cont > 7){
			// desabilita o form
			$('body form').find('input,select').attr("disabled","disabled");
			// limpa o contador
			clearInterval(contagem);
			// redireciona para uma page qualquer
			window.location = "http://www.google.com";
		}
		$('.contador').html(8-cont);
	},1000);
	contagem();
}