/**
 * This should call the modal confirmation for the delete action
 */
$( document ).on('click', '.deletable-register', function (event) 
{
	var id = $( this ).attr('data-id');

	$('input[name=delete_question_id]').val(id);

	return;
});

/**
 * Fired when the #confirm buttom is pressed on the confirmation modal 
 */
$( document ).on('click', '#delete-question-confirm', function(event)
{
	var id = $('input[name=delete_question_id]').val();
	var URL = BASE_URL + '/question/' + id;

	$.ajax({
		url: URL,
		type: 'DELETE'
	}).done(function(result) {
		if( result == 200 )
			$('#line_' + id).fadeOut();
		else
			alert('Falha na exclusão!');
	});
});

/**
 * Arrange the modal to correctly display the achievement information
 */
$( document ).on('click', '.add-modal-register', function(event)
{
	var question_id = $( this ).attr('data-question-id');
	
	$('input[name=question_id]').val(question_id);

	return;
});

/**
 *
 */
$( document ).on('click', '.edit-question-option', function(event)
{
	// Incluir valores das opções.
	var option_id    = $( this ).attr('data-option-id');
	var question_id  = $( this ).attr('data-question-id');
	var option = $( this ).attr('data-option-answer');
	var option_order = $( this ).attr('data-option-order');
	if( $( this ).attr('data-option-blocker') == 'true' )
		var option_is_blocker = 1;
	else
		var option_is_blocker = 0;

	$('#edit-option-answer').val(  option );
	$('#edit-option-order').val( option_order );
	$('#edit-option-blocker').val( option_is_blocker );
	$('input[name=option_id').val( option_id );
	$('input[name=question_id').val( question_id );
});

/**
 * AJAX request when the payload is sent the be stored.
 */
$( document ).on('click', '#add-option-confirm', function(event)
{
	var question_id = $('input[name=question_id]').val();

	var payload = {
		'option'  : $('#option-answer').val(), 
		'order'   : $('#option-order').val(), 
		'blocker' : $('#option-blocker').val(), 
	};

	var URL = BASE_URL + '/question/' + question_id + '/option';

	$.ajax({
		url: URL,
		type: 'POST',
		data: payload,
		success: function (data) {
			if( data == 200 ){
				alert('Opção cadastrada com sucesso!');

				location.reload();
			}
			else
				alert('Erro no cadastro da opção. Tente novamente, se o erro persistir informe ao departamento técnico.');
		},
	});

	return;
});

/**
 * AJAX request when the payload is sent the be updated.
 */
$( document ).on('click', '#edit-option-confirm', function(event)
{
	var question_id = $('input[name=question_id]').val();
	var option_id   = $('input[name=option_id]').val();

	var payload = {
		'option'  : $('#edit-option-answer').val(), 
		'order'   : $('#edit-option-order').val(), 
		'blocker' : $('#edit-option-blocker').val(), 
	};

	console.log(payload);

	var URL = BASE_URL + '/question/' + question_id + '/option/' + option_id + '/edit';

	$.ajax({
		url: URL,
		type: 'POST',
		data: payload,
		success: function (data) {
			if( data == 200 ){
				alert('Opção cadastrada com sucesso!');

				location.reload();
			}
			else 
				alert('Erro no cadastro da opção. Tente novamente, se o erro persistir informe ao departamento técnico.');
		},
	});

	return;
});

/**
 * This should call the modal confirmation for the delete action in the option register.
 */
$( document ).on('click', '.deletable-option-register', function (event) 
{
	var question_id = $( this ).attr('data-question-id');
	var option_id   = $( this ).attr('data-option-id');

	$('input[name=delete_question_id]').val(question_id);
	$('input[name=delete_option_id]'  ).val(option_id);

	return;
});

/**
 * Fired when the #confirm buttom is pressed on the confirmation modal 
 */
$( document ).on('click', '#delete-option-confirm', function(event)
{
	var question_id = $('input[name=delete_question_id]').val();
	var option_id   = $('input[name=delete_option_id]').val();
	
	var URL = BASE_URL + '/question/' + question_id + '/option/' + option_id;

	$.ajax({
		url: URL,
		type: 'DELETE'
	}).done(function(result) {
		if (result == 'false')
			alert('Falha na exclusão!');
		else
			$('#question-option-' + option_id).fadeOut();
	});
});