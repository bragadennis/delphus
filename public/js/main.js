$.fn.globalErrorDisplay = function (error)
{
	$('#show-to-the-it-guy-text-message').text( JSON.stringify(error.responseJSON) );
	$('#show-to-the-it-guy').modal("show");
};

$.fn.putDataInWarningModal = function (operation, resource, verb, payload) {
	if( typeof(operation) !=  undefined )
		$('#modal-operation-span-note').val( operation );

	if( typeof(resource) !=  undefined )
		$('#modal-operation-resource-path').val( resource );

	if( typeof(verb) !=  undefined )
	{
		$('#modal-operation-resource-verb').val( verb );

		if( (verb == 'POST') || (verb == 'PUT') )
			$('#modal-operation-resource-payload').val( payload );
	}
};

$( document ).on('click', '#confirm-danger-modal-operation', function (event) {
	$.ajax({
		url: $('#modal-operation-resource-path').val(), 
		type: $('#modal-operation-resource-verb').val(),
		payload: $('#modal-operation-resource-payload').val(),
		succes: function(response)
		{
			response = $.parseJSON(response);

			if(response.response != 'SUCCESS')
			{
				$('#show-to-the-it-guy-text-message').text( JSON.stringify(response.error) );
				$('#show-to-the-it-guy').modal("show");
			}
		}, 
		error:  function(error)
		{
			$('#show-to-the-it-guy-text-message').text( JSON.stringify(error) );
			$('#show-to-the-it-guy').modal("show");
		}
	});
});