<?php

namespace Delphus\Models;

use Illuminate\Database\Eloquent\Model;

use Delphus\Models\Form;
use Delphus\Models\Option;
use Delphus\Models\LeadAnswer;

class Question extends Model
{
	public static $types = [
        'radio'  => 'Radiobox', 
        'check'  => 'Checkbox', 
        'select' => 'Select', 
        'text'   => 'Texto', 
    ];

    /**
	 * Overrides the convention for the table name.
	 *
	 * @var string
	 */
	// protected $table = 'questions';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	####
	#	Models Relationship
	####

	public function options()
	{
		return $this->hasMany(Option::class);
	}

	public function answers()
	{
		return $this->hasMany(LeadAnswer::class);
	}

	public function form()
	{
		return $this->belongsTo(Form::class);
	}

	#### 
	#	Getters and Setters
	####

	# Relationship
	public function getOptions()
	{
		return $this->options()->get();
	}

	public function getOptionsOnOrder($order = 'asc')
	{
		return $this->options()->orderBy('order', $order)->get();
	}

	public function setOption(Option $option): Question
	{
		$this->options()->attach( $option );

		return $this;
	}

	public function getAnswers()
	{
		return $this->answers()->get();
	}

	public function setAnswers(Answer $answer): Question
	{
		$this->answer()->attach( $answer );

		return $this;
	}

	public function getForm(): Form
	{
		return $this->form()->first();
	}

	public function setForm( Form $form ): Question
	{
		$this->form()->associate( $form );

		return $this;
	}

	# Attributes
	public function getQuestion()
	{
		return $this->question;
	}

	public function setQuestion($question): Question
	{
		$this->question = $question;

		return $this;
	}

	public function getOrder()
	{
		return $this->order;
	}

	public function setOrder($order): Question
	{
		$this->order = $order;

		return $this;
	}

	public function getComprehensiveType()
	{
		$comprehensive_type; 

		switch ($this->type) 
		{
			case 'select':
				$comprehensive_type = 'Select';
				break;
			case 'radio':
				$comprehensive_type = 'Radio Box';
				break;
			case 'check':
				$comprehensive_type = 'Checkbox (multipla escolha)';
				break;
			case 'text':
				$comprehensive_type = 'Campo texto';
				break;
		}

		return $comprehensive_type;
	}

	public function getType()
	{
		return $this->type;
	}

	public function setType($type): Question
	{
		$this->type = $type;

		return $this;
	}

	####
	#	Query Area
	####
	public function getFirstOption()
	{
		if( $this->type != 'text' )
			throw new Exception("Not appliable for the model", 1);
			
		return $this->options()->first();
	}

	####
	#	Static Methods Area
	####

	public static function byOrder( $order )
	{
		return self::where('order', '=', $order)->first();
	}

	public static function ordered($order = 'asc')
	{
		return self::orderBy('order', $order)->get();
	}

}