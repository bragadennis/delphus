<?php

namespace Delphus\Models;

use Illuminate\Database\Eloquent\Model;

use Delphus\Models\Question;
use Delphus\Models\LeadAnswer;

class Option extends Model
{
	/**
	 * Overrides the convention for the table name.
	 *
	 * @var string
	 */
	// protected $table = 'answers';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	####
	#	Models Relationship
	####

	public function question()
	{
		return $this->belongsTo(Question::class);
	}

	public function lead_answers()
	{
		return $this->hasMany(LeadAnswer::class);
	}

	#### 
	#	Getters and Setters
	####

	# Relationship
	public function getQuestion()
	{
		return $this->question()->first();
	}

	public function setQuestion(Question $question): Option
	{
		$this->question()->associate( $question );

		return $this;
	}

	public function getLeadAnswers()
	{
		return $this->lead_answers()->get();
	}

	public function setLeadAnswers(Answer $answer): Option
	{
		$this->lead_answer()->attach( $answer );

		return $this;
	}

	# Attributes
	public function getAnswer()
	{
		return $this->answer;
	}

	public function setAnswer($answer): Option
	{
		$this->answer = $answer;

		return $this;
	}

	public function getOrder()
	{
		return $this->order;
	}

	public function setOrder($order): Option
	{
		$this->order = $order;

		return $this;
	}

	public function isBlocker(): bool
	{
		return $this->blocker;
	}

	public function setBlocker(bool $blocker): Option
	{
		$this->blocker = $blocker;

		return $this;
	}

	####
	#   Static Methods
	####
	public static function byOrder( $order )
	{
		return self::where('order', '=', $order)->get();
	}

	public static function ordered(Question $question,  $order = 'asc')
	{
		return self::where('question_id', '=', $this->question_id)->orderBy('order', $order)->get();
	}
}
