<?php

namespace Delphus\Models;

use Illuminate\Database\Eloquent\Model;

use Delphus\Models\Lead;
use Delphus\Models\Option;
use Delphus\Models\Question;

class LeadAnswer extends Model
{
    /**
	 * Overrides the convention for the table name.
	 *
	 * @var string
	 */
	protected $table = 'answer_lead';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;


	####
	#	Models Relationship
	####

	public function lead()
	{
		return $this->belongsTo(Lead::class);
	}

	public function option()
	{
		return $this->belongsTo(Lead::class);
	}

	public function question()
	{
		return $this->belongsTo(Lead::class);
	}

	#### 
	#	Getters and Setters
	####

	# Relationship
	public function getLead()
	{
		return $this->lead()->first();
	}

	public function setLead(Lead $lead): LeadAnswer
	{
		$this->lead()->associate( $lead );

		return $this;
	}

	public function getOption()
	{
		return $this->option()->first();
	}

	public function setOption(Option $option): LeadAnswer
	{
		$this->option()->associate( $option );

		return $this;
	}

	public function getQuestion()
	{
		return $this->question()->first();
	}

	public function setQuestion(Question $question): LeadAnswer
	{
		$this->question()->associate( $question );

		return $this;
	}

	public function getOpenField()
	{
		return $this->open_field;
	}

	public function setOpenField($open_field): LeadAnswer
	{
		$this->open_field = $open_field;

		return $this;
	}

	####
	#	Static Methods Area
	####
	public static function create(Lead $lead, Question $question, Option $option, $open_field = '')
	{
		$answer = ( new self )->setLead( $lead )
							  ->setQuestion( $question )
							  ->setOption( $option )
							  ->setOpenField( $open_field )
							  ->save();

		return $answer;
	}
}
