<?php

namespace Delphus\Models;

use Illuminate\Database\Eloquent\Model;

use Delphus\Models\LeadAnswer;

class Lead extends Model
{
    /**
	 * Overrides the convention for the table name.
	 *
	 * @var string
	 */
	// protected $table = 'leads';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

	####
	#	Models Relationship
	####

	public function lead_answers()
	{
		return $this->hasMany(LeadAnswer::class);
	}

	#### 
	#	Getters and Setters
	####

	# Relationship
	public function getLeadAnswers()
	{
		return $this->lead_answers()->get();
	}

	public function setLeadAnswer(LeadAnswer $answer): Lead
	{
		$this->lead_answers()->attach( $answer );

		return $this;
	}

	# Attributes
	public function getFirstName()
	{
		return $this->first_name;
	}

	public function setFirstName($first_name): Lead
	{
		$this->first_name = $first_name;

		return $this;
	}

	public function getLastName()
	{
		return $this->last_name;
	}

	public function setLastName($last_name): Lead
	{
		$this->last_name = $last_name;

		return $this;
	}

	public function getFullname()
	{
		return $this->getFirstName() . ' ' . $this->getLastName();
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($email): Lead
	{
		$this->email = $email;

		return $this;
	}

	public function getCellphone()
	{
		return $this->cellphone;
	}

	public function setCellphone($cellphone): Lead
	{
		$this->cellphone = $cellphone;

		return $this;
	}

	public function getCpf()
	{
		return $this->cpf;
	}

	public function setCpf($cpf): Lead
	{
		$this->cpf = $cpf;

		return $this;
	}

	public function getToken()
	{
		return $this->token;
	}

	public function setToken($token): Lead
	{
		$this->token = $token;

		return $this;
	}
	
	public function getVoucher()
	{
		return $this->voucher;
	}

	public function setVoucher($voucher): Lead
	{
		$this->voucher = $voucher;

		return $this;
	}

	public function getValidationToken()
	{
		return $this->validation_token;
	}

	public function setValidationToken($validation_token): Lead
	{
		$this->validation_token = $validation_token;

		return $this;
	}

	public function isEmailConfirmed()
	{
		return $this->confirmed_email;
	}

	public function setEmailConfirmed(bool $confirmed_email): Lead
	{
		$this->confirmed_email = $confirmed_email;

		return $this;
	}

	public function confirmEmail(): Lead
	{
		$this->setEmailConfirmed( true )->save();
		
		return $this;
	}

	public function isBlocked()
	{
		return $this->blocked;
	}

	public function setBlocked(bool $blocked): Lead
	{
		$this->blocked = $blocked;

		return $this;
	}

	public function block(): Lead
	{
		$this->blocked = true;
		$this->save();

		return $this;
	}

	####
	#   Static Methods
	####

	public static function generateToken()
	{
		do {
			 $token = substr(md5(mt_rand()), 0, 128);
		} while( Lead::where('token', '=', $token)->count() > 0 );

		return $token;
	}

	public static function generateVoucher()
	{
		do {
			 $voucher = strtoupper( substr(md5( mt_rand() ), 0, 10) );
		} while( Lead::where('voucher', '=', $voucher)->count() > 0 );

		return $voucher;
	}

	public static function generateEmailToken()
	{
		do {
			 $token = substr(md5(mt_rand()), 0, 128);
		} while( Lead::where('validation_token', '=', $token)->count() > 0 );

		return $token;
	}

	public static function byToken( $token )
	{
		return self::where('token', '=', $token)->first();
	}

	public static function byEmail( $email )
	{
		return self::where('email', '=', $email)->first();
	}

	public static function create($first_name, $last_name, $email, $phone, $cpf)
	{
		$lead = new self;

		$lead->setFirstName( $first_name )
       		 ->setLastName( $last_name )
       		 ->setEmail( $email )
       		 ->setCellphone( $phone )
       		 ->setCpf( $cpf)
       		 ->setToken( Lead::generateToken() )
       		 ->setEmailConfirmed( false )
       		 ->save();

       	return $lead;
	}

	public static function verifyValidation( $validation_token )
	{
		$lead = Lead::where('validation_token', '=', $validation_token)->first();

		// In case the token retuns no lead.
		if( ! isset($lead->id) )
			return [];

		$lead->confirmEmail();

		return $lead;
	}
}