<?php

namespace Delphus\Models;

use Illuminate\Database\Eloquent\Model;

use Delphus\Models\Question;

class Form extends Model
{
    /**
     * Overrides the convention for the table name.
     *
     * @var string
     */
    // protected $table = 'forms';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    ####
    #   Models Relationship
    ####

    public function questions()
    {
        return $this->hasMany(Question::class);
    }


    #### 
    #   Getters and Setters
    ####

    # Relationship
    public function getQuestions()
    {
        return $this->questions()->get();
    }

    public function setQuestion(Question $question): Form
    {
        $this->question()->attach( $question );

        return $this;
    }

    # Attributes
    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number): Form
    {
        $this->number = $number;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): Form
    {
        $this->name = $name;

        return $this;
    }

    ####
    #   Static Methods
    ####
}