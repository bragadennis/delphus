<?php

namespace Delphus\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Delphus\Models\Lead;

class ResendVoucher extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.voucher')
                    ->with([
                        'lead_name' => $this->lead->getFirstName(), 
                        'voucher' => $this->lead->getVoucher()
                    ])
                    ->subject( 'Esse é o seu voucher para concorrer ao prêmio!' );
    }
}
