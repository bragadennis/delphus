<?php

namespace Delphus\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Delphus\Models\Lead;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.verification')
                    ->with([
                        'lead_name' => $this->lead->getFirstName(), 
                        'verification_token' => $this->lead->getValidationToken()
                    ])
                    ->subject( 'E-mail de validação de endereço para a promoção iByte!' );
    }
}
