<?php

namespace Delphus\Http\Controllers;

use Illuminate\Http\Request;

use Delphus\Models\Form;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = Form::orderBy('number', 'asc')->get();

        return view('form.index', ['forms' => $forms, 'script' => $this->scripts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form.edit', ['script' => $this->scripts]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required|max:255', 
            'number' => 'required|integer|unique:forms,number|min:0', 
        ]);

        $form = new Form;

        $form->setName($request->input('name'));
        $form->setNumber($request->input('number'));
        $form->save();

        return redirect("form");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * /
    public function show($id)
    {
        $form = Form::find( $id );

        return view('form.show', ['form' => $form, 
                                  'scripts' => $this->scripts]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form = Form::find( $id );

        return view('form.edit', ['form' => $form, 
                                  'scripts' => $this->scripts]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'name' => 'required|max:255', 
            'number' => 'required', 
        ]);

        $form = Form::find( $id );

        $form->setName($request->input('name'));
        $form->setNumber($request->input('number'));
        $form->save();

        return redirect("form");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return ( Form::destroy($id) ) ? 'true' : 'false';
    }

     #### 
    #   Private Area
    ####

    private $scripts = ['form/crud.js'];
}
