<?php

namespace Delphus\Http\Controllers;

use Illuminate\Http\Request;

use Delphus\Models\Lead;
use Delphus\Models\Form;
use Delphus\Models\Option;
use Delphus\Models\Question;
use Delphus\Models\LeadAnswer;

use Delphus\Mail\VerifyEmail;
use Delphus\Mail\ResendVoucher;

class LandingController extends Controller
{
	/**
	 * Display landing.
	 *
	 * @access Public
	 * @return View
	 */
	public function landing()
	{
		return view('landing.landing');
	}

	/**
	 * Display form for getting lead.
	 *
	 * @access Public
	 * @return View
	 */
	public function lead(Request $request) 
	{
		return view('landing.landing', ['status' => $request->input('status')]);
	}
	
	/**
	 * Validate email for user.
	 *
	 * @access Public
	 * @param Str $token  -  Token lead idenfier.
	 * @return HTTP Redirect
	 */
	public function validateLead($validation_token)
	{
		$lead = Lead::verifyValidation( $validation_token );

		if( ! isset($lead->id) )
			return view('landing.invalid-confirmation');
		
		$lead->setVoucher( Lead::generateVoucher() )->save();

		return redirect("voucher/" . $lead->getToken() );
	}
	
	/**
	 * Block lead. Any furthre attemp to answer the pool again on the same idenfier (email) will be strictly denied.
	 *
	 * @access Public
	 * @param Str $token  -  Token lead idenfier.
	 * @return View
	 */
	public function blockLead($token)
	{
		$lead = Lead::byToken( $token );

		$lead->block();

		return view('landing.blocked-user');
	}
	
	/**
	 * In case the lead trys to answer the pool for a second time under the same e-mail.
	 *
	 * @access Public
	 * @return View
	 */
	public function existingLead(Request $request)
	{
		$view_data = [];

		if( $request->has('status') )
			if ( $request->input('status') == 'INVALID_EMAIL' )
				$view_data['invalid_email'] = 'invalid_email';

		return view('landing.lead-already-on', ['view_data' => $view_data]);
	}
	
	/**
	 * In case a lead request for the voucher to be resent to his email.
	 *
	 * @access Public
	 * @param Request $request  - Lead identifier (email).
	 * @return HTTP Response
	 */
	public function resendVoucher(Request $request)
	{
		$lead = Lead::byEmail( $request->input('email') );

		if( ! $lead )
			return redirect('/lead/existing-lead?status=INVALID_EMAIL');

		// Send confirmation email.
		\Mail::to( $lead->getEmail() )->send( new ResendVoucher( $lead ) );

		return redirect('lead');
	}

	/**
	 * Persist lead provided information on database.
	 *
	 * @access Public
	 * @param Request $request  - Payload sent from lead form.
	 * @return HTTP Redirect
	 */
	public function writeLead(Request $request)
	{

		$validate = $request->validate([
            'fullname' => 'required|string|max:255', 
            'email' => 'required|string|email|max:255', 
            'phone' => 'required|string', 
            'cpf' => 'required|string|max:14', 
        ]);

		$existing_lead = Lead::byEmail($request->input('email'));
		if( isset($existing_lead->id) )
			if( $existing_lead->isBLocked() )
				return redirect('/lead/block/' . $existing_lead->getToken() );
			else 
				return redirect('/lead/existing-lead');

		$fullname = explode(' ', $request->input('fullname'));
		$first_name = array_shift( $fullname );
		$last_name  = implode(' ', $fullname );

        $lead = Lead::create(
        	$first_name, 
        	$last_name, 
        	$request->input('email'), 
        	$request->input('phone'), 
        	$request->input('cpf')
        );

       	// Throw a log for lead acquisition

		return redirect("/questionary/" . $lead->getToken() );
	}

	/**
	 * Display questionary for lead.
	 *
	 * @access Public
	 * @return View
	 */
	public function questionary($token)
	{
		$lead = Lead::byToken( $token );
		if( $lead->isBlocked() )
			return redirect('lead/blocked');

		$questions = Question::ordered();

		// Case it isn't a valid token, redirect user for lead capture page.
		if( ! isset($lead->id) )
			return redirect('/lead?status=invalid_token');

		return view('landing.questionary', ['questions' => $questions, 'lead' => $lead]);
	}

	/**
	 * Persist lead's answers to questionary on database.
	 *
	 * @access Public
	 * @param Request $request  - Payload sent from questionary form.
	 * @return HTTP Redirect
	 */
	public function answerQuestionary(Request $request)
	{
		// echo "<PRE>"; print_r($request->all()); exit;
		$lead = Lead::byToken( $request->input('lead_token') );

		foreach( $request->all() as $question_label => $sent_option )
		{
			if( $question_label == '_token' OR $question_label == 'lead_token' )
				continue;

			if( strpos($question_label, '-extra') )
				continue;

			list($trash, $question_id) = explode('-', $question_label);

			$question = Question::find( $question_id );

			$extra = ( $request->has("question-$question_id-extra") ? $request->input("question-$question_id-extra") : '' );

			// Pending on the question type, a decision must be made on how to proceed.
			switch( $question->getType() )
			{
				// Unique possible answer
				case 'radio':
				case 'select':
					$option = Option::find( $sent_option );

					LeadAnswer::create($lead, $question, $option, $extra);
					break;
					
				// Multiple possible answers
				case 'check':
					if( ! is_array($sent_option) )
						$sent_option = [$sent_option];

					foreach( $sent_option as $opt_id )
					{
						$option = Option::find( $opt_id );

						LeadAnswer::create($lead, $question, $option, $extra);
					}
					break;

				// Text to be afixed on intermediary table
				case 'text';
					$option = $question->getFirstOption();

					LeadAnswer::create($lead, $question, $option, $sent_option , $extra);
					break;
			}
		}

		$lead->setValidationToken( Lead::generateEmailToken() )->save();

		// Send confirmation email.
		\Mail::to( $lead->getEmail() )->send( new VerifyEmail( $lead ) );

		return view('landing.verify-mail');

		// return redirect('/lead/existing-lead');
	}

	/**
	 * Display voucher with feasible string, being the identification for lead on sort.
	 *
	 * @access Public
	 * @param  Str $token  - Token for lead identification. This token is provided upon email confirmation.
	 * @return View.
	 */
	public function displayVoucher($token)
	{
		$lead = Lead::byToken( $token );

		if( $lead->isEmailConfirmed() != true )
			return redirect("questionary/$token");

		return view('landing.voucher', ['lead' => $lead]);
	}
}