<?php

namespace Delphus\Http\Controllers;

use Illuminate\Http\Request;

use Delphus\Models\Form;
use Delphus\Models\Option;
use Delphus\Models\Question;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        return view( 'question.index', ['questions' => $questions,
                                        'scripts'   => $this->scripts] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $forms = [];
        foreach( Form::all() as $form)
            $forms[$form->id] = $form->id . ' - ' . $form->getName();

        return view('question.edit', ['forms' => $forms, 
                                      'types' => Question::$types, 
                                      'scripts'   => $this->scripts]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'form_id' => 'required|integer', 
            'question' => 'required|string|max:255', 
            'order' => 'required|integer|unique:questions,order', 
            'type' => 'required', 
        ]);

        $question = new Question;
        $form = Form::find( $request->input('form_id') );

        $question->setForm($form);
        $question->setQuestion($request->input('question'));
        $question->setType($request->input('type'));

        if( $request->input('order') != '' )
            $question->setOrder( $request->input('order') );
        else 
        {
            $top_order = Question::orderBy('order', 'desc')->first();

            $question->setOrder( ++$top_order->order );
        }

        $question->save();

        return redirect("question/$question->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = Question::find( $id );

        return view('question.show', ['question' => $question, 
                                      'scripts'  => $this->scripts]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find( $id );

        $forms = [];
        foreach( Form::all() as $form)
            $forms[$form->id] = $form->id . ' - ' .$form->getName();

        return view('question.edit', ['question' => $question, 
                                      'forms'    => $forms,
                                      'types'    => Question::$types,
                                      'scripts'  => $this->scripts]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'form_id' => 'required|integer', 
            'question' => 'required|string|max:255', 
            'order' => 'required|integer', 
            'type' => 'required', 
        ]);

        $question = Question::find( $id );
        $form = Form::find( $request->input('form_id') );

        $question->setForm( $form );
        $question->setQuestion( $request->input('question') );
        $question->setType( $request->input('type') );

        if( $request->input('order') != '' )
            $question->setOrder($request->input('order'));

        $question->save();

        return redirect("question/$question->id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return ( Question::destroy($id) ) ? 'true' : 'false';
    }

    /**
     * Create an option for a given question.
     *
     * @access Public
     * @param Request $request  - Post payload holder
     * @param Int $question_id  - Question database identifier
     * @return Str - 'true' if succeed, 'false' if it doesn't
     */
    public function addOption(Request $request, $question_id)
    {
        $validate = $request->validate([
            'option' => 'required|string|max:255', 
            'order' => 'required|integer', 
            'blocker' => 'required|boolean', 
        ]);

        $question = Question::find( $question_id );
        $option   = new Option;

        $option->setQuestion( $question );
        $option->setAnswer(  $request->input('option') );
        $option->setOrder(   $request->input('order') );
        $option->setBlocker( $request->input('blocker') );

        if( $option->save() )
            return response(200);
        else 
            return response(['status' => 'UNABLE_TO_SAVE'], 422);
    }

    /**
     * Create an option for a given question.
     *
     * @access Public
     * @param Request $request  - Post payload holder
     * @param Int $question_id  - Question database identifier
     * @param Int $option_id  - OPtion database identifier
     * @return Str - 'true' if succeed, 'false' if it doesn't
     */
    public function editOption(Request $request, $question_id, $option_id)
    {
        $validate = $request->validate([
            'option' => 'required|string|max:255', 
            'order' => 'required|integer', 
            'blocker' => 'required|boolean', 
        ]);

        $question = Question::find( $question_id );
        $option   = Option::find( $option_id);

        $option->setQuestion( $question );
        $option->setAnswer(  $request->input('option') );
        $option->setOrder(   $request->input('order') );
        $option->setBlocker( $request->input('blocker') );

         if( $option->save() )
            return response(200);
        else 
            return response(['status' => 'UNABLE_TO_SAVE'], 422);
    }

    /**
     * Remove an specific option from the database.
     *
     * @param  int  $option_id  - Option identifier on database
     * @return Str - 'true' the operation succeeds, 'false' if it doesn't
     */
    public function deleteOption($question_id, $option_id)
    {
        return ( Option::destroy( $option_id ) ) ? 'true' : 'false';
    }

    #### 
    #   Private Area
    ####

    private $scripts = ['question/crud.js'];
}
