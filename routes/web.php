<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::get('/login', function () {
	if( ! Auth::guest() )
		return redirect('/dashboard');
	else 
		return redirect('/login');
});

Auth::routes();

// Route::group(['middleware' => ['auth', 'web']], function() {
Route::group(['middleware' => ['web']], function() {
	  Route::get('/dashboard', 'DashboardController@dashboard');
	// Resourceful controllers
	  Route::resource('/form', 'FormController')->except(['show']);
	  Route::resource('/question', 'QuestionController');
	  Route::post('question/{question_id}/option',    'QuestionController@addOption');
	  Route::post('question/{question_id}/option/{option_id}/edit',   'QuestionController@editOption');
	  Route::delete('question/{question_id}/option/{option_id}', 'QuestionController@deleteOption');
});

Route::group(['middleware' => ['web']], function() {
	// Landing routes
	  Route::get ('landing',               'LandingController@landing');
	  Route::get ('lead',                  'LandingController@lead');
	  Route::get ('lead/validate/{token}', 'LandingController@validateLead');
	  Route::get ('lead/block/{token}',    'LandingController@blockLead');
	  Route::get ('lead/existing-lead',    'LandingController@existingLead');
	  Route::post('lead/resend-voucher',   'LandingController@resendVoucher');
	  Route::post('lead',                  'LandingController@writeLead');
	  Route::get ('questionary/{token}',   'LandingController@questionary');
	  Route::post('questionary',           'LandingController@answerQuestionary');
	  Route::get ('voucher/{token}',       'LandingController@displayVoucher');
});
