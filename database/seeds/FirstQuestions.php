<?php

use Illuminate\Database\Seeder;

use Delphus\Models\Form;
use Delphus\Models\Question;
use Delphus\Models\Option;

class FirstQuestions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = [
        		['order' => 1, 
        		 'type' => 'select', 
        		 'question' => 'Você, alguém de sua casa ou parente trabalha ou trabalhou em alguma dessas áreas?', 
	        	 'options' => [
	        		['answer' => 'a - Agência de Propaganda ou Publicidade / Relações Públicas', 'order' => 1, 'blocker' => true],
	        		['answer' => 'b - Instituto de Pesquisa de Mercado', 'order' => 2, 'blocker' => true],
	        		['answer' => 'c – Loja de produtos de Informática', 'order' => 3, 'blocker' => true],
	        		['answer' => 'd - Jornal / Revista / TV / Rádio', 'order' => 4, 'blocker' => true],
	        		['answer' => 'Nenhuma destas', 'order' => 5, 'blocker' => false]]
	        	],
        		['order' => 2, 
        		 'type' => 'select', 
        		 'question' => 'Onde você mora?', 
	        	 'options' => [
	        		['answer' => 'Belém', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Ananindeua', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Benevides', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Marituba', 'order' => 4, 'blocker' => false],
	        		['answer' => 'Santa Izabel do Pará', 'order' => 5, 'blocker' => false],
	        		['answer' => 'Castanhal', 'order' => 6, 'blocker' => false],
	        		['answer' => 'Outro', 'order' => 7, 'blocker' => true]],
	        	],
        		['order' => 3, 
        		 'type' => 'radio', 
        		 'question' => 'Qual o seu gênero?', 
	        	 'options' => [
	        		['answer' => 'Masculino', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Feminino',  'order' => 2, 'blocker' => false]],
	        	],
        		['order' => 4, 
        		 'type' => 'text', 
        		 'question' => 'Qual a sua idade?', 
	        	 'options' => [
	        		['answer' => 'age', 'order' => 1, 'blocker' => false]],
	        	],
        		['order' => 5, 
        		 'type' => 'select', 
        		 'question' => 'Qual é o seu grau de instrução?', 
	        	 'options' => [
	        		['answer' => 'Ensino fundamental', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Ensino médio', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Superior incompleto', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Superior completo/Pós-graduação', 'order' => 4, 'blocker' => false]],
	        	],
	        	['order' => 6, 
        		 'type' => 'select', 
        		 'question' => 'Qual das opções abaixo retrata sua situação de trabalho atual?', 
	        	 'options' => [
	        		['answer' => 'Emprego formal (com carteira assinada)', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Emprego informal (sem carteira assinada)', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Profissional autônomo', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Emprego formal e emprego informal', 'order' => 4, 'blocker' => false],
	        		['answer' => 'Estou desempregado', 'order' => 5, 'blocker' => false],
	        		['answer' => 'Vivo de renda', 'order' => 6, 'blocker' => false]],
	        	],
        		['order' => 7, 
        		 'type' => 'select', 
        		 'question' => 'Em qual das opções abaixo retrata sua renda familiar mensal?', 
	        	 'options' => [
	        		['answer' => 'Até 1 salário mínimo', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Entre 1 e 2 salários', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Entre 2 e 5 salários', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Entre 5 e 10 salários', 'order' => 4, 'blocker' => false],
	        		['answer' => 'Acima de 10 salários mínimos', 'order' => 5, 'blocker' => false],
	        		['answer' => 'Não tenho renda', 'order' => 6, 'blocker' => false]],
	        	],
        		['order' => 8,
        		 'type' => 'radio', 
        		 'question' => 'Você utiliza ou compra produtos de tecnologia no seu dia a dia?', 
	        	 'options' => [
	        		['answer' => 'Sim', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Não', 'order' => 2, 'blocker' => false]],
	        	],
        		['order' => 9, 
        		 'type' => 'check', 
        		 'question' => 'Quais produtos de tecnologia MAIS UTILIZA no seu dia a dia?', 
	        	 'options' => [
	        		['answer' => 'Desktop/ Computador de mesa', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Notebook', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Televisão (sem internet)', 'order' => 3, 'blocker' => false],
	        		['answer' => 'SmartTV', 'order' => 4, 'blocker' => false],
	        		['answer' => 'Smartphone (celular com acesso à internet)', 'order' => 5, 'blocker' => false],
	        		['answer' => 'Celular (sem acesso à internet)', 'order' => 6, 'blocker' => false],
	        		['answer' => 'Bateria portátil', 'order' => 7, 'blocker' => false],
	        		['answer' => 'Netbook', 'order' => 8, 'blocker' => false],
	        		['answer' => 'Tablet', 'order' => 9, 'blocker' => false],
	        		['answer' => 'GPS', 'order' => 10, 'blocker' => false],
	        		['answer' => 'Impressoras', 'order' => 11, 'blocker' => false],
	        		['answer' => 'Monitores', 'order' => 12, 'blocker' => false],
	        		['answer' => 'Acessórios, periféricos e sortimentos de informática', 'order' => 13, 'blocker' => false],
	        		['answer' => 'Mp3, Mp4, Mp5', 'order' => 14, 'blocker' => false],
	        		['answer' => 'Câmeras Digitais', 'order' => 15, 'blocker' => false],
	        		['answer' => 'Filmadoras', 'order' => 16, 'blocker' => false],
	        		['answer' => 'Projetores Multimídia', 'order' => 17, 'blocker' => false],
	        		['answer' => 'Home Theather/ Sound Bar', 'order' => 18, 'blocker' => false],
	        		['answer' => 'Caixa de som portátil ', 'order' => 19, 'blocker' => false],
	        		['answer' => 'Fones de ouvido, Headset', 'order' => 20, 'blocker' => false],
	        		['answer' => 'Porta Retrato Digital', 'order' => 21, 'blocker' => false],
	        		['answer' => 'Placas de Rede, Placa Mãe, Placa de Vídeo, Processador', 'order' => 22, 'blocker' => false],
	        		['answer' => 'Estabilizadores', 'order' => 23, 'blocker' => false],
	        		['answer' => 'Roteador, Switch, Hub, Modens', 'order' => 24, 'blocker' => false],
	        		['answer' => 'Vídeo Games e Jogos', 'order' => 25, 'blocker' => false],
	        		['answer' => 'Streaming de mídias (Chromecast, Apple TV)', 'order' => 26, 'blocker' => false]],
	        	],
        		['order' => 10, 
        		 'type' => 'check', 
        		 'question' => 'Quais destes produtos de tecnologia você COMPROU no último ano?', 
	        	 'options' => [
	        		['answer' => 'Desktop/ Computador de mesa', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Notebook', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Televisão (sem internet)', 'order' => 3, 'blocker' => false],
	        		['answer' => 'SmartTV', 'order' => 4, 'blocker' => false],
	        		['answer' => 'Smartphone (celular com acesso à internet)', 'order' => 5, 'blocker' => false],
	        		['answer' => 'Celular (sem acesso à internet)', 'order' => 6, 'blocker' => false],
	        		['answer' => 'Bateria portátil', 'order' => 7, 'blocker' => false],
	        		['answer' => 'Netbook', 'order' => 8, 'blocker' => false],
	        		['answer' => 'Tablet', 'order' => 9, 'blocker' => false],
	        		['answer' => 'GPS', 'order' => 10, 'blocker' => false],
	        		['answer' => 'Impressoras', 'order' => 11, 'blocker' => false],
	        		['answer' => 'Monitores', 'order' => 12, 'blocker' => false],
	        		['answer' => 'Acessórios, periféricos e sortimentos de informática', 'order' => 13, 'blocker' => false],
	        		['answer' => 'Mp3, Mp4, Mp5', 'order' => 14, 'blocker' => false],
	        		['answer' => 'Câmeras Digitais', 'order' => 15, 'blocker' => false],
	        		['answer' => 'Filmadoras', 'order' => 16, 'blocker' => false],
	        		['answer' => 'Projetores Multimídia', 'order' => 17, 'blocker' => false],
	        		['answer' => 'Home Theather/ Sound Bar', 'order' => 18, 'blocker' => false],
	        		['answer' => 'Caixa de som portátil ', 'order' => 19, 'blocker' => false],
	        		['answer' => 'Fones de ouvido, Headset', 'order' => 20, 'blocker' => false],
	        		['answer' => 'Porta Retrato Digital', 'order' => 21, 'blocker' => false],
	        		['answer' => 'Placas de Rede, Placa Mãe, Placa de Vídeo, Processador', 'order' => 22, 'blocker' => false],
	        		['answer' => 'Estabilizadores', 'order' => 23, 'blocker' => false],
	        		['answer' => 'Roteador, Switch, Hub, Modens', 'order' => 24, 'blocker' => false],
	        		['answer' => 'Vídeo Games e Jogos', 'order' => 25, 'blocker' => false],
	        		['answer' => 'Streaming de mídias (Chromecast, Apple TV)', 'order' => 26, 'blocker' => false]],
	        	],
        		['order' => 11, 
        		 'type' => 'text', 
        		 'question' => 'Quando falamos de local para comprar <strong>tecnologia</strong>, qual a primeira palavra que vem à mente?', 
	        	 'options' => [
	        		['answer' => 'place', 'order' => 1, 'blocker' => false]],
	        	],
        		['order' => 12, 
        		 'type' => 'text', 
        		 'question' => 'Quais lojas de tecnologia você conhece mesmo que seja só de ouvir falar?', 
	        	 'options' => [
	        		['answer' => 'store', 'order' => 1, 'blocker' => false]],
	        	],
        		['order' => 13, 
        		 'type' => 'text', 
        		 'question' => 'Qual loja de tecnologia você <strong>mais compra</strong>?', 
	        	 'options' => [
	        		['answer' => 'buy_more_from', 'order' => 1, 'blocker' => false]],
	        	],
        		['order' => 14, 
        		 'type' => 'text', 
        		 'question' => 'Em qual loja você comprou o seu <strong>último</strong> produto de tecnologia?', 
	        	 'options' => [
	        		['answer' => 'last_bought_from', 'order' => 1, 'blocker' => false]],
	        	],
        		['order' => 15, 
        		 'type' => 'check', 
        		 'question' => 'Onde costuma realizar as compras de produto de tecnologia?', 
	        	 'options' => [
	        		['answer' => 'Lojas físicas', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Internet / Lojas online', 'order' => 2, 'blocker' => false]],
	        	],
        		['order' => 16, 
        		 'type' => 'check', 
        		 'question' => 'Costuma pesquisar sobre produtos de tecnologia antes da compra? Se sim, em quais meios?', 
	        	 'options' => [
	        		['answer' => 'Não pesquiso', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Amigos/Familiares', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Loja física', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Sites de lojas de tecnologia', 'order' => 4, 'blocker' => false],
	        		['answer' => 'YouTube', 'order' => 5, 'blocker' => false],
	        		['answer' => 'Facebook', 'order' => 6, 'blocker' => false],
	        		['answer' => 'Instagram', 'order' => 7, 'blocker' => false],
	        		['answer' => 'Blogs/ Sites especializados em tecnologia', 'order' => 8, 'blocker' => false],
	        		['answer' => 'Panfletos', 'order' => 9, 'blocker' => false],
	        		['answer' => 'Revista', 'order' => 10, 'blocker' => false]],
	        	],
        		['order' => 17, 
        		 'type' => 'select', 
        		 'question' => 'Em relação ao seu comportamento de compra de produtos de tecnologia, com qual das opções abaixo você mais se identifica?', 
	        	 'options' => [
	        		['answer' => 'Pesquiso na internet, mas prefiro comprar na loja física', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Pesquiso e compro na loja física', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Pesquisa e compro na loja virtual', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Pesquiso na loja física e compro na loja virtual', 'order' => 4, 'blocker' => false]],
	        	],
        		['order' => 18, 
        		 'type' => 'radio', 
        		 'question' => 'Serviço de assistência técnica dentro da própria loja.', 
	        	 'options' => [
	        		['answer' => 'não importante', 'order' => 1, 'blocker' => false],
	        		['answer' => 'pouco importante', 'order' => 2, 'blocker' => false],
	        		['answer' => 'importante', 'order' => 3, 'blocker' => false],
	        		['answer' => 'muito importante', 'order' => 4, 'blocker' => false],
	        		['answer' => 'extremamente importante', 'order' => 5, 'blocker' => false]],
	        	],
        		['order' => 19, 
        		 'type' => 'radio', 
        		 'question' => 'Oferta de serviços de manutenção, instalação e configuração de computadores, impressoras e redes sem fio dentro da própria loja.', 
	        	 'options' => [
	        		['answer' => 'não importante', 'order' => 1, 'blocker' => false],
	        		['answer' => 'pouco importante', 'order' => 2, 'blocker' => false],
	        		['answer' => 'importante', 'order' => 3, 'blocker' => false],
	        		['answer' => 'muito importante', 'order' => 4, 'blocker' => false],
	        		['answer' => 'extremamente importante', 'order' => 5, 'blocker' => false]],
	        	],
        		['order' => 20, 
        		 'type' => 'radio', 
        		 'question' => 'Comprar produtos de tecnologia no site e retirar o produto na loja física.', 
	        	 'options' => [
	        		['answer' => 'não importante', 'order' => 1, 'blocker' => false],
	        		['answer' => 'pouco importante', 'order' => 2, 'blocker' => false],
	        		['answer' => 'importante', 'order' => 3, 'blocker' => false],
	        		['answer' => 'muito importante', 'order' => 4, 'blocker' => false],
	        		['answer' => 'extremamente importante', 'order' => 5, 'blocker' => false]],
	        	],
        		['order' => 21, 
        		 'type' => 'check', 
        		 'question' => 'Quais shoppings você mais frequenta?', 
	        	 'options' => [
	        		['answer' => 'Shopping a', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Shopping b', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Shopping c', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Shopping d', 'order' => 4, 'blocker' => false],
	        		['answer' => 'Nenhum / Não frequento shoppings', 'order' => 5, 'blocker' => false]],
	        	],
        		['order' => 22, 
        		 'type' => 'radio', 
        		 'question' => 'Qual shoppings é o seu preferido?', 
	        	 'options' => [
	        		['answer' => 'Shopping a', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Shopping b', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Shopping c', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Shopping d', 'order' => 4, 'blocker' => false]],
	        	],
        		['order' => 23, 
        		 'type' => 'radio', 
        		 'question' => 'Em qual shopping você mais compra?', 
	        	 'options' => [
	        		['answer' => 'Shopping a', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Shopping b', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Shopping c', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Shopping d', 'order' => 4, 'blocker' => false]],
	        	],
        		['order' => 24, 
        		 'type' => 'check', 
        		 'question' => 'Que atividades costuma realizar nos momentos de lazer?', 
	        	 'options' => [
	        		['answer' => 'Praia', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Futebol/Jogar bola', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Igreja', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Shopping', 'order' => 4, 'blocker' => false],
	        		['answer' => 'Ver TV', 'order' => 5, 'blocker' => false],
	        		['answer' => 'Ficar em casa', 'order' => 6, 'blocker' => false],
	        		['answer' => 'Casa de amigos ou família', 'order' => 7, 'blocker' => false],
	        		['answer' => 'Barzinhos', 'order' => 8, 'blocker' => false],
	        		['answer' => 'Cinema', 'order' => 9, 'blocker' => false]],
	        	],
        		['order' => 25, 
        		 'type' => 'radio', 
        		 'question' => 'Você lembra de ter visto, ouvido ou lido alguma propaganda de lojas de tecnologia no último mês?', 
	        	 'options' => [
	        		['answer' => 'Sim', 'order' => 1, 'blocker' => false],
	        		['answer' => 'Não', 'order' => 2, 'blocker' => false]],
	        	],
        		['order' => 26, 
        		 'type' => 'check', 
        		 'question' => 'Em quais meios de comunicação você viu, ouviu ou leu propagandas de lojas de tecnologia?', 
	        	 'options' => [
	        		['answer' => 'TV aberta', 'order' => 1, 'blocker' => false],
	        		['answer' => 'TV fechada', 'order' => 2, 'blocker' => false],
	        		['answer' => 'Outdoor', 'order' => 3, 'blocker' => false],
	        		['answer' => 'Busdoor (traseira de ônibus)', 'order' => 4, 'blocker' => false],
	        		['answer' => 'Rádio', 'order' => 5, 'blocker' => false],
	        		['answer' => 'Jornal', 'order' => 6, 'blocker' => false],
	        		['answer' => 'Redes sociais', 'order' => 7, 'blocker' => false],
	        		['answer' => 'Panfletos', 'order' => 8, 'blocker' => false]],
	        	],
        ];

    	$form = new Form;
    	$form->setName('Pesquisa IByte.')
    		 ->setNumber(1)
    		 ->save();

        foreach( $questions as $question )
        {
        	$q = new Question;
        	$q->setQuestion($question['question'])
        	  ->setOrder($question['order'])
        	  ->setType($question['type'])
        	  ->setForm( $form )
        	  ->save();

        	foreach( $question['options'] as $option )
        	{
        		$opt = new Option;

        		$opt->setAnswer( $option['answer'] )
        			->setOrder( $option['order'] )
        			->setBlocker( $option['blocker'] )
        			->setQuestion( $q )
        			->save();
        	}
        }

        echo "Done.\n";

        return true;
    }
}