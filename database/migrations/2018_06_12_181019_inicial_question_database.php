<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InicialQuestionDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function( Blueprint $table )
        {
            $table->increments('id');
            $table->integer('number')->unique();
            $table->string('name');
        });

        Schema::create('questions', function( Blueprint $table )
        {
            $table->increments('id');
            $table->integer('form_id')->unsigned();
            $table->string('question');
            $table->integer('order')->unique(); // Should tell in which the questions should be performed
            $table->enum('type', ['radio', 'check', 'select', 'text']);

            $table->foreign('form_id')->references('id')->on('forms')->onDelete('cascade');
        });

        Schema::create('options', function( Blueprint $table )
        {
            $table->increments('id');
            $table->integer('question_id')->unsigned();
            $table->string('answer');
            $table->integer('order');
            $table->boolean('blocker')->default(false);

            $table->unique(['question_id', 'order']);

            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });

        Schema::create('leads', function( Blueprint $table )
        {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('cellphone')->nullable();
            $table->string('cpf');
            $table->string('token', 128)->nullable()->unique();
            $table->string('voucher', 50)->nullable()->unique();
            $table->boolean('blocked')->default( false )->nullable();

            $table->boolean('confirmed_email')->default( false )->nullable();
            $table->string('validation_token', 128)->nullable()->unique();
        });

        Schema::create('answer_lead', function( Blueprint $table )
        {
            $table->increments('id');
            $table->integer('lead_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->integer('option_id')->unsigned();
            $table->string('open_field')->nullable();

            $table->timestamps();

            // The lead and question's ID should be unique, since the each lead
            //    should answer each question only once.
            $table->unique(['lead_id', 'option_id']);

            $table->foreign('lead_id')->references('id')->on('leads')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_lead');
        Schema::dropIfExists('leads');
        Schema::dropIfExists('options');
        Schema::dropIfExists('questions');
        Schema::dropIfExists('forms');
    }
}
